/**
 * https://bitbucket.org/MaxXx1313/ucoin/src/master/scripts/proxy.js
 */

/**
 * @description
 * Proxy server which can help to overcome CORS restrictions
 *
 * @usage
 ```bash
 npm install express body-parser cors request
 node proxy.js
 ```
 */

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');
const request = require('request');
const PORT = process.env.PORT || 3128;

const PROXY_HEADER = 'X-Request-To';
console.log('To make request passed through the proxy:\n\t1. Set desired url to "%s" header\n\t2. Make request (GET, POST, etc.) to "/proxy" endpoint', PROXY_HEADER);

function rawBodyParser(opts) {
  return function (req, res, next) {
    req.rawBody = '';
    req.setEncoding('utf8');

    req.on('data', function (chunk) {
      req.rawBody += chunk;
    });

    req.on('end', function () {
      next();
    });
  }
}

const app = express();

// log
app.use(function (req, res, next) {
  console.log('%s\t%s', req.method.toUpperCase(), req.url);
  next();
});

app.use(cors());
// app.use(bodyParser.json()); // for parsing application/json
app.use(rawBodyParser());

// respond to proxy requests
app.use('/proxy', proxy);


//create node.js http server and listen on port
const server = http.createServer(app).listen(PORT, function () {
  console.log('Proxy server started at %s', server.address().port);
});


/**
 *
 */
function proxy(req, res) {
  const method = req.method.toLowerCase();
  const targetUrl = req.get(PROXY_HEADER);
  // const targetUrl = 'http://localhost:9090'; // DEBUG
  // console.log(req.rawBody);

  if (targetUrl) {
    // console.log(req.headers);
    // console.log(req.body);

    // pass headers
    const passHeaders = [
      'accept',
      'accept-language',
      'accept-encoding',
      'content-type',
      'content-length',
      'user-agent',
    ];
    const headers = {};
    for (let i = 0; i < passHeaders.length; i++) {
      const h = passHeaders[i];
      if (req.headers[h]) {
        headers[h] = req.headers[h];
      }
    }

    const opts = {
      method: method,
      url: targetUrl,
      headers: headers,
      // body:req.body,
      // json:true
    };
    if (method === 'post') {
      opts.body = req.rawBody;
      // opts.json = true;
    }

    const target = request(opts);
    // req.pipe(target);
    target.pipe(res);
  } else {
    res.status(400).send({ok: false, message: 'Invalid header: ' + PROXY_HEADER});
  }
}

// // DEBUG
// var app2 = express();
// app2.use(rawBodyParser()); // for parsing application/json
// app2.use(function(req, res, next){
//   console.log('DEBUG SERVER: %s\t%s', req.method.toUpperCase(), req.url);
//   console.log('\theaders: ', req.headers);
//   console.log('\tbody    : ', req.rawBody);
//   res.send('ok');
// });
// //create node.js http server and listen on port
// var server2 = http.createServer(app2).listen(9090, function(){
//   console.log('Server2 started at %s', server2.address().port);
// });
