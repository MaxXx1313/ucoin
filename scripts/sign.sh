#!/bin/bash

releaseFolder="release"

filename=$(ls $releaseFolder/)
keystore="my-release-key.keystore"
keypass="changeit"

toolsLatest=$(ls "$ANDROID_HOME/build-tools" -t |head -n 1)
#certId=$(keytool -list -keystore "$keystore" -alias alias_name -storepass "$keypass" |grep fingerprint | grep -oP '\w{2}:\w{2}:\w{2}:\w{2}' |head -n 1 |sed s/://g)


jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore "$keystore" "$releaseFolder/$filename" alias_name -signedjar "$releaseFolder/$filename" -storepass "$keypass"
"$ANDROID_HOME/build-tools/$toolsLatest/zipalign" -v 4 "$releaseFolder/$filename" "$releaseFolder/${filename%.*}-signed.${filename##*.}"

