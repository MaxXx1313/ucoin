#!/bin/bash



function checkapp(){
  local app="$1"
  #local installCommand="$2"
  which "$app";
  if [ $? -ne 0 ]; then
      echo "Error: $app is not installed"
      #echo -e "Run:\tapt install libxml-xpath-perl"
      exit 1
  fi;
}


checkapp xpath
checkapp jarsigner


# checkapp zipalign
toolsLatest=$(ls "$ANDROID_HOME/build-tools" -t |head -n 1)
PATH="$PATH:$ANDROID_HOME/build-tools/$toolsLatest"
checkapp zipalign
