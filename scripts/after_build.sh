#!/bin/bash


name=$(xpath -q -e '//widget/@id' config.xml |grep -oP '".*"' |sed s/\"//g)
version=$(xpath -q -e '//widget/@version' config.xml |grep -oP '".*"' |sed s/\"//g)
build=$(date +%s)

# env > env.txt
if [ "$npm_lifecycle_event" = "build" ]; then
  rm -rf target
  mkdir -p target
  cp "platforms/android/app/build/outputs/apk/debug/app-debug.apk" "target/$name-$version-$build.apk"
fi;

if [ "$npm_lifecycle_event" = "release" ]; then
  rm -rf release
  mkdir -p release
  cp "platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk" "release/$name-$version.apk"
fi;


# jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore release/by.maxxx1313-0.1.1.apk alias_name -storepass "changeit"
# /home/maksim/.android/build-tools/28.0.1/zipalign -v 4 release/by.maxxx1313-0.1.1.apk release/by.maxxx1313-0.1.1-aligned.apk
