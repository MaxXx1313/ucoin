import { Pipe, PipeTransform } from '@angular/core';

type Args = 'keyval' | 'key' | 'value';

// https://stackoverflow.com/questions/31490713/iterate-over-object-in-angular
@Pipe({
  name: 'mapToIterable',
  pure: false
})
export class MapToIterablePipe implements PipeTransform {
  transform(obj: {} = {}, arg: Args = 'keyval') {
    switch (arg) {
      case 'keyval' :
        return Object.keys(obj).map(key => ({ key: key, value: obj[key] }));
      case 'key' :
        return Object.keys(obj);
      case 'value' :
        return Object.keys(obj).map(key => obj[key]);
      default:
        console.warn('MapToIterablePipe: invalid argument:', arg);
        return null;
    }
  }
} // -
