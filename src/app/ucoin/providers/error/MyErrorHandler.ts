import { Injectable } from '@angular/core';
import { AlertController, IonicErrorHandler } from "ionic-angular";


@Injectable()
export class MyErrorHandler extends IonicErrorHandler {
  /**
   *
   */
  constructor(
    private alert: AlertController,
  ) {
    super();
  }

  /**
   * @param error
   */
  handleError(error: any): void {
    console.error('MyErrorHandler: handleError', error);
    error = error || {};

    if (error.rejection) {
      error = error.rejection;
    }

    if (error.status == 0) {
      error.message = 'Network is not available';
    }

    const alert = this.alert.create({
      title: 'Error',
      message: error.message || error || 'Unknown error',
      buttons: ['Ok']
    });

    // strange but it doesn't appear properly without timeout
    setTimeout(() => {
      try {
        alert.present();
      }catch(e){
        console.error('FATAL ERROR', e);
      }
    });


  }

} // -
