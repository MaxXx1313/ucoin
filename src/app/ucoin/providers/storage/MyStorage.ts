import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IStorage } from "./IStorage";

/**
 * A simple settings/config class for storing key/value pairs with persistence.
 */
@Injectable()
export class MyStorage implements IStorage {
  private SETTINGS_PREFIX: string = 'settings_';

  /**
   *
   */
  constructor(
    public storage: Storage,
  ) {
    // this._defaults = defaults;
  }

  get(key: string): Promise<any> {
    return this.storage.get(this.SETTINGS_PREFIX + key)
  }

  set(key: string, value: any) {
    return this.storage.set(this.SETTINGS_PREFIX + key, value);
  }

} // -
