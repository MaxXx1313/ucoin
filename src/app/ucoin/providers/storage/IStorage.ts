/**
 *
 */
export interface IStorage {
  /**
   * @param {string} key
   * @returns {Promise<any>}
   */
  get(key: string): Promise<any>

  /**
   * @param {string} key
   * @param value
   * @returns {Promise<any>}
   */
  set(key: string, value: any): Promise<any>

} // -
