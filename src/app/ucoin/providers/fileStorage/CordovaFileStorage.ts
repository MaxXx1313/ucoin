import { Injectable } from '@angular/core';
// import { File } from '@ionic-native/file';
import { IFileStorage } from "./IFileStorage";



interface FileSystem {
  root: DirectoryEntry;
  name: "persistent" | "temporary";
}


interface MyFilePlugin {
  applicationDirectory: string  // "file:///android_asset/",

  applicationStorageDirectory: string;  // "file:///data/user/0/io.ionic.starter/",
  dataDirectory: string;                // "file:///data/user/0/io.ionic.starter/files/",
  cacheDirectory: string;               // "file:///data/user/0/io.ionic.starter/cache/",

  externalRootDirectory: string;  // "file:///storage/emulated/0/",
  externalDataDirectory: string;  // "file:///storage/emulated/0/Android/data/io.ionic.starter/files/",
  externalCacheDirectory: string; // "file:///storage/emulated/0/Android/data/io.ionic.starter/cache/",
}

interface FileError extends Error {
  code: number;
}


/**
 *
 */
export enum FileErrorType {
  NOT_FOUND_ERR = 1,
  SECURITY_ERR = 2,
  ABORT_ERR = 3,
  NOT_READABLE_ERR = 4,
  ENCODING_ERR = 5,
  NO_MODIFICATION_ALLOWED_ERR = 6,
  INVALID_STATE_ERR = 7,
  SYNTAX_ERR = 8,
  INVALID_MODIFICATION_ERR = 9,
  QUOTA_EXCEEDED_ERR = 10,
  TYPE_MISMATCH_ERR = 11,
  PATH_EXISTS_ERR = 12
}

/**
 *
 */
declare enum LocalFileSystem {
  TEMPORARY = 0,
  PERSISTENT = 1
}


interface FSEntry {
  fullPath: string;
  isDirectory: boolean;
  isFile: boolean;
  nativeURL: string;

  filesystem: FileSystem;

  // files only?
  toInternalURL: () => string;
  toURL: () => string;
}


// replacement for '@ionic-native/file'
interface DirectoryEntry extends FSEntry {
  getFile: (filename: string, opts: EntryOptions, onSuccess: (entry?: DirectoryEntry) => any, onError: (e?: Error) => any) => void;
  getDirectory: (foldername: string, opts: EntryOptions, onSuccess: (entry?: DirectoryEntry) => any, onError: (e?: Error) => any) => void;
  removeRecursively: (onSuccess: () => any, onError: (e?: Error) => any) => void;

  name: string;
}


declare interface FileEntry extends FSEntry {
  file: any;
  createWriter: (onSuccess: (fileWriter: any /* FileWriter */) => any, onError: (e?: Error) => any) => void;
}


interface EntryOptions {
  create?: boolean;
  exclusive?: boolean; // file only
}

/**
 * https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html
 * https://github.com/apache/cordova-plugin-file#list-of-error-codes-and-meanings
 */
@Injectable()
export class CordovaFileStorage implements IFileStorage {


  // private fs: FileSystem;
  // private file: MyFilePlugin;


  // internal
  // protected get basePathInternal(): string {
  //   return cordova.file.applicationDirectory + 'www/';
  // }

  /**
   *
   */
  constructor(
    // private file: File,
  ) {

  }

  // sd-card
  protected get basePathExternal(): string {
    return window['cordova'].file.externalApplicationStorageDirectory + 'files/';
  }

  /**
   *
   */
  ready(): Promise<any> {
    return Promise.resolve();
    // return new Promise((resolve, reject) => {

    //   this.file = window['cordova'].file as MyFilePlugin;

    //   window['requestFileSystem'](LocalFileSystem.PERSISTENT, /* size */ 0, (fs) => {
    //     console.log('File: file system opened: ', fs);
    //     window['fs'] = fs;

    //     this.fs = fs as FileSystem;
    //     resolve(fs);
    //   }, reject);

    // });

  }

  /**
   *
   */
  requestPermissions(): Promise<any> {
    return Promise.resolve();
    // return new Promise((resolve, reject) => {
    //   window['requestFileSystem'](LocalFileSystem.PERSISTENT, 0, (fileSystem) => {
    //     this.fileSystem = fileSystem;
    //     resolve(this.fileSystem);
    //   }, reject);
    // });
  }


  /**
   * @param path
   */
  readFile(path: string): Promise<string> {
    return this.getFileEntry(path, {create: false}).then(fileEntry => {
      return this._readFile(fileEntry);
    });
  }

  /**
   * @param path
   */
  getFileUrl(path: string): Promise<string> {
    return this.getFileEntry(path, {create: false}).then(fileEntry => fileEntry.nativeURL);
    // return this.getFileEntry(location).then(fileEntry => fileEntry.toInternalURL());
    // return this.getFileEntry(location).then(fileEntry => fileEntry.toURL());
  }

  /**
   * @param {string} path
   * @param {string | Blob | ArrayBuffer} data
   * @returns {Promise<string>} file location
   */
  writeFile(path: string, data: ArrayBuffer | string): Promise<string> {
    console.log('CordovaFileStorage.writeFile', path);
    // const [_, folder, filename] = path.match(/^(.*)\/(.+?)$/);
    return this.getFileEntry(path, {create: true}).then(fileEntry => {
      return this._writeFile(fileEntry, data).then(() => {
        return fileEntry.nativeURL;
      });
    });
  }

  /**
   * @returns {Promise<any>}
   */
  removeFolder(path: string): Promise<any> {
    console.log('CordovaFileStorage.removeFolder:', path);

    return this.getFolderEntry(path, {create: false}).then(dirEntry => {
      return new Promise((resolve, reject) => {
        dirEntry.removeRecursively(resolve, reject);
      });
    });

  }


  /**
   * @param path
   * @param opts
   */
  getFolderEntry(path: string, opts: EntryOptions): Promise<DirectoryEntry> {
    if (opts.create) {
      return this._createFolderEntryRecursively(path);
    } else {
      return this._getFolderEntry(path);
    }
  }


  /**
   * @param path
   * @param opts
   */
  getFileEntry(path: string, opts: EntryOptions): Promise<FileEntry> {

    const [_, folderName, fileName] = path.match(/^(.*)\/(.*?)$/) || [null, null, null];
    return this.getFolderEntry(folderName, opts).then(folderEntry => {
      return new Promise((resolve, reject) => {
        folderEntry.getFile(fileName, opts, resolve, reject);
      });
    });
  }


  /**
   * @param {string} path
   * @private
   */
  protected _getFolderEntry(path: string): Promise<DirectoryEntry> {

    // transform relative path to absolute
    if (!path.startsWith(this.basePathExternal)) {
      path = this.basePathExternal + path;
    }

    return new Promise((resolve, reject) => {
      window['resolveLocalFileSystemURL'](path, (fileEntry) => {
        resolve(fileEntry as DirectoryEntry);
      }, reject);
    });
  }


  /**
   * Create folder(s) recursively
   */
  protected _createFolderEntryRecursively(path: string): Promise<DirectoryEntry> {
    console.log("CordovaFileStorage._createEntryRecursively:", path);

    // remove basePath
    if (path.startsWith(this.basePathExternal)) {
      path = path.substring(this.basePathExternal.length + 1);
    }

    const pathParts = path.replace(/\/+$/, '').replace(/^\/+/, '').split('/');
    return this.getFolderEntry(this.basePathExternal, {create: false}).then(rootEntry => {

      return new Promise((resolve, reject) => {
        let currentEntry = rootEntry;

        function _step(): void {
          const currentDir = pathParts.shift();
          if (!currentDir) {
            setTimeout(() => {
              resolve(currentEntry)
            });
            return;
          }

          currentEntry.getDirectory(currentDir, {create: true}, newParent => {
            currentEntry = newParent;
            setTimeout(_step);
          }, reject);
        }

        _step();
      });
    });
  }


  /**
   *
   */
  private _writeFile(fileEntry: FileEntry, fileData: ArrayBuffer | string): Promise<any> {
    console.log("File: writeFile: ", fileEntry);

    return new Promise((resolve, reject) => {
      // Create a FileWriter object for our FileEntry (log.txt).
      fileEntry.createWriter((fileWriter) => {

        fileWriter.onwriteend = function () {
          console.log("File: Successful file write", fileEntry);
          resolve();
        };

        fileWriter.onerror = function (e) {
          console.log("Failed file write: ", e);
          reject(e);
        };

        fileWriter.write(new Blob([fileData]));
      }, reject);
    });
  }


  /**
   *
   */
  private _readFile(fileEntry: FileEntry): Promise<string> {
    console.log("File: _readFile: ", fileEntry);

    return new Promise((resolve, reject) => {
      fileEntry.file(function (file) {
        const reader = new FileReader();

        reader.onloadend = function () {
          // console.log('Successful file read: ' + this.result);
          resolve(this.result as string);
        };
        reader.readAsText(file);
      }, reject);
    }) as Promise<string>;
  }

} // -
