import { Injectable } from '@angular/core';
import { IFileStorage } from "./IFileStorage";
import { xpath_get, xpath_set } from "../../../../utils";



/**
 * A simple settings/config class for storing key/value pairs with persistence.
 */
@Injectable()
export class MemoryFileStorage implements IFileStorage {

  private _storage: object = {};

  /**
   *
   */
  constructor() {
    console.warn('MemoryStorage: using this provider is for debug purpose only (window._mem_storage)!!');
    window['_mem_storage'] = this._storage;
  }

  /**
   *
   */
  ready(): Promise<any> {
    try {
      this._storage = JSON.parse(localStorage['storage']);
    } catch (e) {
      console.warn('MemoryStorage: Local Storage data is corrupted or not exists');
      this._storage = {};
    }
    console.log('MemoryStorage: ready');
    return Promise.resolve();
  }

  /**
   * @param {string} path
   * @param {string | Blob | ArrayBuffer} content
   * @returns {Promise<string>}
   */
  writeFile(path: string, content: string | Blob | ArrayBuffer): Promise<string> {
    console.log('MemoryFileStorage: writeFile', path);

    xpath_set(this._storage, path, content ? String(content) : null, '/');

    this._saveState();
    return Promise.resolve(path);
  }


  /**
   * @param {string} path
   * @returns {Promise<string>}
   */
  readFile(path: string): Promise<string | Blob | ArrayBuffer> {
    console.log('MemoryFileStorage: readFile', path);

    const content = xpath_get(this._storage, path, '/');
    return content ? Promise.resolve(String(content)) : Promise.reject('No such file: ' + path);
  }


  /**
   * @returns {Promise<any>}
   */
  removeFolder(dirName: string): Promise<any> {
    console.log('MemoryFileStorage: removeFolder', dirName);
    const pathParts = dirName.split('/');
    const lastName = pathParts.pop();
    const path = pathParts.join('/');

    const folder = xpath_get(this._storage, path, '/');
    delete folder[lastName];

    this._saveState();
    return Promise.resolve();
  }

  /**
   * @private
   */
  protected _saveState(){
    localStorage['storage'] = JSON.stringify(this._storage);
  }

} // -
