/**
 *
 */
export interface IFileStorage {

  ready(): Promise<any>;

  readFile(path: string): Promise<string | Blob | ArrayBuffer>;
  writeFile(path: string, content: string | Blob | ArrayBuffer): Promise<string>;

  removeFolder(path: string): Promise<any>;

} // -
