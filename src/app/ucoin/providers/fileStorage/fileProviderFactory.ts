import { Platform } from "ionic-angular";
import { MemoryFileStorage } from "./MemoryFileStorage";
import { CordovaFileStorage } from "./CordovaFileStorage";
import { IFileStorage } from "./IFileStorage";


export abstract class FileStorage implements IFileStorage {

  abstract ready(): Promise<any>;

  abstract readFile(path: string): Promise<string | Blob | ArrayBuffer>;
  abstract writeFile(path: string, content: string | Blob | ArrayBuffer): Promise<string>;

  abstract removeFolder(path: string): Promise<any>;
} // -


export const fileProviderFactory = {
  provide: FileStorage,
  deps: [Platform],
  useFactory: function fileProviderFactoryFn(platform: Platform) {

    if (platform.is('core')) {
      console.log('FileStorage: using MemoryStorage');
      return new MemoryFileStorage();
    } else {
      console.log('FileStorage: using FileStorage');
      return new CordovaFileStorage(/*file*/);
    }

  }
};

