import { EventEmitter, Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from "rxjs";



export const SettingsKey = {
  FOLLOW_USER: 'followUser',
};


/**
 * A simple settings/config class for storing key/value pairs with persistence.
 */
@Injectable()
export class Settings {
  settings: any;
  _defaults: any;


  /**
   * Event emitter
   */
  protected _events: {
    [key: string]: EventEmitter<any>
  } = {};

  // _readyPromise: Promise<any>;
  private SETTINGS_KEY: string = '_settings';

  constructor(public storage: Storage, defaults: any) {
    this._defaults = defaults;
  }

  get allSettings() {
    return this.settings;
  }

  ready() {
    return this.storage.get(this.SETTINGS_KEY).then((value) => {
      if (value) {
        this.settings = value;
        return this._mergeDefaults(this._defaults);
      } else {
        return this.setAll(this._defaults).then((val) => {
          this.settings = val;
        })
      }
    });
  }

  _mergeDefaults(defaults: any) {
    for (let k in defaults) {
      if (!(k in this.settings)) {
        this.settings[k] = defaults[k];
      }
    }
    return this.setAll(this.settings);
  }

  merge(settings: any) {
    for (let k in settings) {
      this.settings[k] = settings[k];
    }
    return this.save();
  }

  setValue(key: string, value: any): Promise<any> {
    this.settings[key] = value;
    return this.storage.set(this.SETTINGS_KEY, this.settings).then(res => {
      this.trigger(key, value);
      return res;
    });
  }

  getValue(key: string): Promise<any> {
    return this.storage.get(this.SETTINGS_KEY)
      .then(settings => {
        return settings[key];
      });
  }

  save() {
    return this.setAll(this.settings);
  }

  /**
   * @param key
   */
  onChange(key: string): Observable<any> {
    if (!this._events[key]) {
      this._events[key] = new EventEmitter();
    }
    return this._events[key];
  }

  /**
   * @param value
   */
  protected setAll(value: any) {
    return this.storage.set(this.SETTINGS_KEY, value);
  }

  /**
   * @param key
   * @param value
   */
  protected trigger(key: string, value: any) {
    if (this._events[key]) {
      this._events[key].emit(value);
    }
  }

} // -
