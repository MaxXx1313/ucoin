import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { EventEmitter, Injectable } from '@angular/core';

import { Country } from '../../models/country';
import { Coin, CoinPeriod, CoinType } from '../../models/coin';
import { UCoinHtmlDataProvider } from './data/ucoin-html-data.service';
import { Observable } from "rxjs/Observable";
import { UCoinLocalDataProvider } from "./data/ucoin-local-data.service";
import { FileStorage } from "../fileStorage/fileProviderFactory";
import { xpath_delete, xpath_get, xpath_set } from "../../../../utils";
import { User, UserId } from "../../models/user";
import { Settings, SettingsKey } from "../settings/settings";



/**
 *
 */
export interface CacheProgressEvent {
  /**
   * Cache ID.
   * In future we can have multiple cache processes run simultaneously
   */
  id: string;

  /**
   * Define active state (active = true when cache process is running
   */
  active: boolean;

  /**
   * progress info
   */
  current?: number;
  total?: number;

  /**
   * optional message
   */
  message?: string;
}


/**
 *
 */
interface CacheMetadata {
  [countryId: string]: {
    [periodId: string]: {
      [coinType: number]: CacheEntry;

      // 1?: boolean; // regular
      // 2?: boolean; // commemorative
      // 3?: boolean; // collector
    };
  };
}

interface CacheEntry {
  users: UserId[]; // user ids
}


interface CacheJob {
  id: string;
  period: CoinPeriod;
  type: CoinType;
  users?: UserId[]; // user ids
  toCache: boolean;
}


/**
 * TODO: use Observables!
 */
@Injectable()
export class UCoinCacheManager {

  readonly CACHE_META_FILE = 'meta/cache.json';

  public readonly cacheProgress: EventEmitter<CacheProgressEvent> = new EventEmitter();

  private followUsersCache: User[] = [];
  private users$: EventEmitter<User[]> = new EventEmitter();

  private cacheMeta: CacheMetadata = {};

  private _cachePool: Array<CacheJob> = [];
  private _currentJob: CacheJob;

  /**
   */
  constructor(
    public fs: FileStorage,
    private ucoinHtml: UCoinHtmlDataProvider,
    private ucoinLocal: UCoinLocalDataProvider,
    private settings: Settings,
  ) {

    this.settings.getValue(SettingsKey.FOLLOW_USER).then(user => {
      this.followUsersCache = user || [];
      this.users$.emit(this.followUsersCache);
    });
  }

  /**
   * Load cache metadata
   * @returns {Promise<any>}
   */
  public async ready(): Promise<any> {
    // this.settings.onChange(SettingsKey.FOLLOW_USER).subscribe();

    return this._getJsonFromFile(this.CACHE_META_FILE).toPromise()
      .then((cacheMeta: CacheMetadata) => {
        this.cacheMeta = cacheMeta || {};
        console.log('UCoinCacheManager: data loaded');
      })
      .catch(e => {
        console.log('UCoinCacheManager: data not loaded', e);
        this.cacheMeta = {};
      });
  }

  /**
   * @param {CoinPeriod} period
   * @param {CoinType} type
   * @param {boolean} toCache
   */
  public setPeriodCacheMode(period: CoinPeriod, type: CoinType, toCache: boolean): void {
    const userIds = this.followUsersCache.map(user => user.id);
    const id = period.country + '_' + period.id + '_' + type + '_' + Math.floor(Math.random() * 100); // eventId

    // push caching jop to pool
    this._cachePool.push({id, period, type, toCache, users: userIds});
    this.digest();
  }


  /**
   *
   */
  public isCountryCached(countryId: string): boolean {
    return !!xpath_get(this.cacheMeta, [countryId]);
  }

  /**
   *
   */
  public isPeriodCached(countryId: string, periodId: string): boolean {
    return !!xpath_get(this.cacheMeta, [countryId, periodId]);
  }

  /**
   *
   */
  public isCoinTypeCached(countryId: string, periodId: string, type: CoinType): boolean {
    return !!xpath_get(this.cacheMeta, [countryId, periodId, type as any]);
  }


  /**
   *
   */
  public async digest(): Promise<void> {
    if (this._currentJob) {
      return; // already in progress
    }

    this._currentJob = this._cachePool.shift();
    if (!this._currentJob) {
      return; // no job
    }

    if (this._currentJob.toCache) {
      await this._addToCache(this._currentJob);
      if (this._currentJob.users) {
        await this._updateUserCoins(this._currentJob);
      }
    } else {
      await this._removeFromCache(this._currentJob);
    }
    //


    // update meta when process is finished!
    await this._updateCacheMeta(this._currentJob.period, this._currentJob.type, this._currentJob.toCache);
    this.cacheProgress.next({id: this._currentJob.id, active: false, message: 'Done'});
    this._currentJob = null;

    this.digest();
  }

  /**
   *
   */
  public addCacheUser(user: User) {
    // this.followUsersCache.push(user);
    // TODO: so far only one user is supported
    this.followUsersCache = [user];

    this.settings.setValue(SettingsKey.FOLLOW_USER, this.followUsersCache);
    this.users$.emit(this.followUsersCache);
  }

  /**
   *
   */
  public removeCacheUser(userId: UserId): void {
    const i = this.followUsersCache.findIndex(user => user.id === userId);
    if (i >= 0) {
      this.followUsersCache.splice(i, 1);

      this.settings.setValue(SettingsKey.FOLLOW_USER, this.followUsersCache);
      this.users$.emit(this.followUsersCache);
    }
  }

  /**
   *
   */
  public getCacheUsers(): Observable<User[]> {
    //TODO: we can use 'SubjectBehaviour' after update rxjs
    return this.users$.startWith(this.followUsersCache);
  }

  /**
   * @private
   */
  protected async _addToCache(job: CacheJob): Promise<void> {
    const id = job.id;
    const countryId = job.period.country;
    const coinCountry = {id: countryId} as Country;
    const coinPeriod = {
      country: countryId,
      id: job.period.id
    } as CoinPeriod;
    const coinType = job.type;

    // this.cacheProgress.next({id, active: true, message: 'Checking cache'});

    this.cacheProgress.next({id, active: true, message: 'Fetching metadata'});
    // const usersToCache = this._getUsersToCache(period, type, users);

    // update counties
    const countries = await this.ucoinHtml.countries().toPromise();
    await this.ucoinLocal.saveCountries(countries);

    // update country periods
    const periods = await this.ucoinHtml.periods(coinCountry).toPromise();
    await this.ucoinLocal.savePeriods(coinCountry, periods);

    // update coins general info
    const coins = await this.ucoinHtml.coins(coinPeriod, coinType).toPromise();
    await this.ucoinLocal.saveCoins(coinPeriod, coinType, coins);


    // update coins details
    for (let i = 0; i < coins.length; i++) {
      this.cacheProgress.next({id, active: true, current: i + 1, total: coins.length, message: 'Caching coins'});

      const coin = coins[i];
      coin.image1 = await this.downloadCoinImage(coin.image1, coin, 0);
      coin.image2 = await this.downloadCoinImage(coin.image2, coin, 1);

      // update coins details
      const coinDetails = await this.ucoinHtml.coinDetails(coin).toPromise();
      await this.ucoinLocal.saveCoinDetails(coin, coinDetails);
    }

    //
    this.cacheProgress.next({id, active: false, message: 'Done'});
  };


  /**
   * @private
   */
  protected async _updateUserCoins(job: CacheJob): Promise<void> {
    this.cacheProgress.next({id: job.id, active: true, message: 'Updating user coins: ' + job.users[0]});
    // TODO: single user only
    const coins = await this.ucoinHtml.coins({
      country: job.period.country,
      id: job.period.id
    } as CoinPeriod, job.type, job.users[0]).toPromise();
    await this.ucoinLocal.saveCoins({country: job.period.country, id: job.period.id} as CoinPeriod, job.type, coins);

  }

  /**
   * @private
   */
  protected async _removeFromCache(job: CacheJob): Promise<void> {
    await this.ucoinLocal.removeCoins({country: job.period.country, id: job.period.id} as CoinPeriod, job.type);
    await this.fs.removeFolder([job.period.country, job.period.id, job.type].join('/'));
  }


  /**
   * @param {CoinPeriod} period
   * @param {CoinType} type
   * @param {boolean} toCache
   */
  protected _updateCacheMeta(period: CoinPeriod, type: CoinType, toCache: boolean): Promise<any> {
    this.cacheMeta = this.cacheMeta || {};
    if (toCache) {
      xpath_set(this.cacheMeta, [period.country, period.id, type as any], true);
    } else {
      xpath_delete(this.cacheMeta, [period.country, period.id, type as any]);
    }

    return this._writeJsonToFile(this.CACHE_META_FILE, this.cacheMeta).toPromise();
  }


  /**
   * Return users which are not in cache
   * @param {CoinPeriod} period
   * @param {CoinType} type
   * @param {UserId[]} userIds
   */
  protected _getUsersToChache(period: CoinPeriod, type: CoinType, userIds: UserId[]): UserId[] {
    this.cacheMeta = this.cacheMeta || {};
    const coinCacheMeta: CacheEntry = xpath_get(this.cacheMeta, [period.country, period.id, type as any]);
    const cachedUsers = (coinCacheMeta || {} as any).users || [];

    return userIds.filter(uid => cachedUsers.indexOf(uid) < 0);
  }


  /**
   * Download image.
   * @return Promise<string> url of the file new location (on filesystem)
   */
  protected downloadCoinImage(imageUrl: string, coin: Coin, index: number): Promise<string> {
    if (!imageUrl) {
      return Promise.resolve(imageUrl);
    }
    if (imageUrl.startsWith('file://')) {
      // local file
      return Promise.resolve(imageUrl);
    }

    const imageId = coin.id, countryId = coin.country, periodId = coin.period, type = coin.type;
    const filename = imageId + '_sm_' + index + '.jpg';

    return this.ucoinHtml.downloadImage(imageUrl).toPromise()
      .then(filedata => {
        return this.fs.writeFile([countryId, periodId, type, filename].join('/'), filedata);
      })
      .catch(e => {
        console.warn('Ucoin: downloadImage error', e);
        return imageUrl;
      });
  }

  /**
   *
   */
  // deletePeriodTypeImages(countryId: string, periodId: string, type: CoinType): Promise<any> {
  //   return this.fs.removeFolder([countryId, periodId, type].join('/'));
  // }


  /**
   * @param {string} filename
   * @private
   */
  protected _getJsonFromFile(filename: string): Observable<any> {
    return Observable.fromPromise(this.fs.readFile(filename).then(data => JSON.parse(data.toString())));
  }

  /**
   * @param {string} filename
   * @param {any} data
   * @private
   */
  protected _writeJsonToFile(filename: string, data: any): Observable<any> {
    return Observable.fromPromise(this.fs.writeFile(filename, JSON.stringify(data)));
  }

} // -
