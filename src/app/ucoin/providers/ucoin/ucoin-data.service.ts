import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Country } from '../../models/country';
import { Coin, CoinDetails, CoinPeriod, CoinType } from '../../models/coin';
import { User } from '../../models/user';
import { SearchResult } from '../../models/SearchResult';
import { UCoinHtmlDataProvider } from './data/ucoin-html-data.service';
import { MyStorage } from '../storage/MyStorage';
import { UCoinLocalDataProvider } from "./data/ucoin-local-data.service";
import { IUCoinDataProvider } from "./data/IUCoinDataProvider";
import { Observable } from "rxjs";
import { ObservablePriorityLast } from "../../../../utils";
import { UCoinCacheManager } from "./ucoin-cache.service";
import { Settings, SettingsKey } from "../settings/settings";
import { fromPromise } from "rxjs/observable/fromPromise";



/**
 *
 */
@Injectable()
export class UCoinData implements IUCoinDataProvider {

  /**
   */
  constructor(
    public storage: MyStorage,
    private ucoinHtml: UCoinHtmlDataProvider,
    private ucoinLocal: UCoinLocalDataProvider,
    private ucoinCache: UCoinCacheManager,
    private settings: Settings,
  ) {
  }

  /**
   * Load cache metadata
   * @returns {Promise<any>}
   */
  public ready(): Promise<any> {
    return Promise.resolve();
  }


  /**
   * @returns {Observable<Array<Country>>}
   */
  countries(): Observable<Array<Country>> {
    return ObservablePriorityLast<Array<Country>>([
      this.ucoinLocal.countries(),
      this.ucoinHtml.countries(),
    ]).map((countries: Array<Country>) => {
      for (let i = 0; i < countries.length; i++) {
        countries[i].availableLocal = this.ucoinCache.isCountryCached(countries[i].id);
      }
      return countries;
    });
  }

  /**
   *
   * @param {Country} country
   * @returns {Observable<Array<CoinPeriod>>}
   */
  periods(country: Country): Observable<Array<CoinPeriod>> {

    return ObservablePriorityLast<Array<CoinPeriod>>([
      this.ucoinLocal.periods(country),
      this.ucoinHtml.periods(country),
    ]).map((periods: Array<CoinPeriod>) => {
      for (let i = 0; i < periods.length; i++) {
        const period = periods[i];
        period.availableLocal = this.ucoinCache.isPeriodCached(country.id, period.id);

        //
        period.cachePolicy = {};
        period.cachePolicy.regular = this.ucoinCache.isCoinTypeCached(country.id, period.id, CoinType.CIRCULAR);
        period.cachePolicy.collector = this.ucoinCache.isCoinTypeCached(country.id, period.id, CoinType.COLLECTOR);
        period.cachePolicy.commemorative = this.ucoinCache.isCoinTypeCached(country.id, period.id, CoinType.COMMEMORATIVE);
      }
      return periods;
    });
  }


  /**
   *
   */
  coins(period: CoinPeriod, type: CoinType): Observable<Array<Coin>> {

    return fromPromise(this.settings.getValue(SettingsKey.FOLLOW_USER))
      .map(users => users[0])
      .flatMap(user => {
        const userId = user ? user.id : null;

        return ObservablePriorityLast<Array<Coin>>([
          this.ucoinLocal.coins(period, type, userId),
          this.ucoinHtml.coins(period, type, userId),
        ]);
      });
  }


  /**
   * @returns {Observable<CoinDetails>}
   */
  coinDetails(coin: Coin): Observable<CoinDetails> {
    return ObservablePriorityLast<CoinDetails>([
      this.ucoinLocal.coinDetails(coin),
      this.ucoinHtml.coinDetails(coin),
    ]);
  }


  /**
   * Find user
   * @param searchString
   * @returns {Observable<User[]>}
   */
  findUser(searchString: string): Observable<SearchResult<User>> {
    return ObservablePriorityLast<SearchResult<User>>([
      // this.ucoinLocal.findUser(searchString), // offline not supported for that operation
      this.ucoinHtml.findUser(searchString),
    ]);
  }


} // -
