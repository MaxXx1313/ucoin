import { Country } from "../../../models/country";
import { Coin, CoinDetails, CoinPeriod, CoinType } from "../../../models/coin";
import { User, UserId } from "../../../models/user";
import { SearchResult } from "../../../models/SearchResult";
import { Observable } from "rxjs";



export interface IUCoinDataProvider {

  /**
   * Get country list from ucoin
   * @returns {Observable<Array<Country>>}
   */
  countries: () => Observable<Array<Country>>;


  /**
   * Get country periods list from ucoin
   * @param {Country} country
   * @returns {Observable<Array<CoinPeriod>>}
   */
  periods: (country: Country) => Observable<Array<CoinPeriod>>;

  /**
   * Get coins list from ucoin
   * @param {CoinPeriod} period - period
   * @param {CoinType} type - coin type
   * @param {UserId} [userId] - user
   * @returns {Observable<Array<Coin>>}
   */
  coins: (period: CoinPeriod, type: CoinType, userId?: UserId) => Observable<Array<Coin>>;


  /**
   * Get coins list from ucoin
   * @param {Coin} coin
   * @returns {Observable<Array<Coin>>}
   */
  coinDetails: (coin: Coin) => Observable<CoinDetails>;


  /**
   * Find user
   * @param string
   */
  findUser: (searchString: string) => Observable<SearchResult<User>>;
}
