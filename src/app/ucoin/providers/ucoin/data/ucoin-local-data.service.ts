import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Country } from '../../../models/country';
import { Coin, CoinDetails, CoinPeriod, CoinType } from '../../../models/coin';
import { User, UserId } from '../../../models/user';
import { Log } from "../../../../../decorators/Log";
import { IUCoinDataProvider } from "./IUCoinDataProvider";
import { Observable } from "rxjs";
import { FileStorage } from "../../fileStorage/fileProviderFactory";
import { OfflineData } from "../../../models/offlineData";



@Injectable()
export class UCoinLocalDataProvider implements IUCoinDataProvider {

  public readonly BASE_FOLDER: string = 'meta/';

  /**
   */
  constructor(
    public fs: FileStorage,
  ) {
  }

  /**
   * Get country list from ucoin
   * @returns {Promise<Array<Country>>}
   */
  @Log()
  countries(): Observable<Array<Country>> {
    const filename = 'countries.json';
    return this._getJsonFromFile(this.BASE_FOLDER + filename);
      // .map(this._processLocalRecords);
  }

  /**
   *
   */
  @Log()
  saveCountries(countries: Array<Country>): Observable<any> {
    const filename = 'countries.json';
    return this._writeJsonToFile(this.BASE_FOLDER + filename, countries);
  }

  /**
   * Get country periods list from ucoin
   * @param {Country} country
   * @returns {Promise<Array<CoinPeriod>>}
   */
  @Log()
  periods(country: Country): Observable<Array<CoinPeriod>> {
    const filename = `periods-${country.id}.json`;
    return this._getJsonFromFile(this.BASE_FOLDER + filename);
      // .map(this._processLocalRecords);
  }

  /**
   *
   */
  @Log()
  savePeriods(country: Country, periods: Array<CoinPeriod>): Observable<any> {
    const filename = `periods-${country.id}.json`;
    return this._writeJsonToFile(this.BASE_FOLDER + filename, periods);
  }


  /**
   * Get coins list from ucoin
   * @param {CoinPeriod} period - period
   * @param {CoinType} type - coin type
   * @param {UserId} [userId] - coin type
   * @returns {Promise<Array<Coin>>}
   */
  @Log()
  coins(period: CoinPeriod, type: CoinType, userId?: UserId): Observable<Array<Coin>> {
    const filename = [period.country, period.id, type, `coins-${period.id}-${type}.json`].join('/');
    return this._getJsonFromFile(this.BASE_FOLDER + filename);
      // .map(this._processLocalRecords);
  }

  /**
   *
   */
  @Log()
  saveCoins(period: CoinPeriod, type: CoinType, coins: Array<Coin>): Observable<any> {
    const filename = [period.country, period.id, type, `coins-${period.id}-${type}.json`].join('/');
    return this._writeJsonToFile(this.BASE_FOLDER + filename, coins);
  }

  /**
   *
   */
  @Log()
  removeCoins(period: CoinPeriod, type: CoinType): Observable<any> {
    const folder = [period.country, period.id, type].join('/');
    return Observable.fromPromise(this.fs.removeFolder(this.BASE_FOLDER + folder));
  }

  /**
   * Get coins list from ucoin
   * @param {Coin} coin
   * @returns {Promise<Array<Coin>>}
   */
  @Log()
  coinDetails(coin: Coin): Observable<CoinDetails> {
    const filename = [coin.country, coin.period, coin.type, `coins-${coin.id}.json`].join('/');
    return this._getJsonFromFile(this.BASE_FOLDER + filename);
      // .map((coin: CoinDetails) => {
      //   this._processLocalRecords([coin]);
      //   return coin;
      // });
  }

  /**
   *
   */
  @Log()
  saveCoinDetails(coin: Coin, coinDetails: CoinDetails): Observable<any> {
    const filename = [coin.country, coin.period, coin.type, `coins-${coin.id}.json`].join('/');
    return this._writeJsonToFile(this.BASE_FOLDER + filename, coinDetails);
  }


  /**
   * Find user
   */
  findUser(searchString: string): Observable<any> {
    throw new Error('Not supported in offline');
  }


  /**
   * @param {Array<any>} records
   * @private
   */
  protected _processLocalRecords<T extends OfflineData>(records: Array<T>): Array<T> {
    for (let i = 0; i < records.length; i++) {
      records[i].availableLocal = true;
    }
    return records;
  }


  /**
   * @param {string} filename
   * @private
   */
  protected _getJsonFromFile(filename: string): Observable<any> {
    return Observable.fromPromise(this.fs.readFile(filename).then(data => JSON.parse(data.toString())));
  }

  /**
   * @param {string} filename
   * @param {any} data
   * @private
   */
  protected _writeJsonToFile(filename: string, data: any): Observable<any> {
    return Observable.fromPromise(this.fs.writeFile(filename, JSON.stringify(data)));
  }

} // -
