/**
 *
 */
import { UCoinHtmlDataProvider } from './ucoin-html-data.service';
import { Coin, CoinDetails, CoinType } from '../../../models/coin';
import { User } from '../../../models/user';

describe('ucoin-html-data', () => {

  // https://en.ucoin.net/catalog
  it('_parseCatalogCountries', () => {
    return fetch('/base/test-resources/catalog.html')
      .then(resp => resp.text())
      .then(function (filedata) {
        var coutries = UCoinHtmlDataProvider._parseCatalogCountries(filedata);
        let coutry12 = { id: 'australia', name: 'Australia', coinsCount: 735 };

        expect(coutries[12]).toEqual(coutry12);
        expect(coutries.length).toEqual(318);
      });
  });


  // it('_parseCatalogCountries (online)', () => {
  //   return fetch('https://en.ucoin.net/catalog')
  //     .then(resp => resp.text())
  //     .then(function (filedata) {
  //       var coutries = UcoinData._parseCatalogCountries(filedata);
  //       let coutry12 = { id: 'australia', name: 'Australia', coinsCount: 735 };

  //       expect(coutries[12]).toEqual(coutry12);
  //       expect(coutries.length).toEqual(318);
  //     });
  // });



  it('_parseCoinPeriods', () => {
    // return fetch('/base/test-resources/catalog_country=russia.html')
    return fetch('/base/test-resources/catalog_tree.txt')
      .then(resp => resp.text())
      .then(function (filedata) {
        var periods0 = UCoinHtmlDataProvider._parseCoinPeriods(filedata, 'russia');
        let periods01 = { id: '12', country: 'russia', name: 'Российская Федерация', dates: '1997 - 2018' };

        expect(periods0[0]).toEqual(periods01);
        expect(periods0.length).toEqual(15);


        // alderney - has just one period
        var periods1 = UCoinHtmlDataProvider._parseCoinPeriods(filedata, 'alderney');
        let periods11 = { id: '622', country: 'alderney', name: 'Королева Елизавета II', dates: '1989 - 2016' };

        expect(periods1[0]).toEqual(periods11);
        expect(periods1.length).toEqual(1);

      });
  });

  // https://en.ucoin.net/table/?country=germany&period=1&type=1&uid=30175&variety=on&mode=print
  it('(regular) _parseCoinsRegular', () => {
    return fetch('/base/test-resources/table__country=germany&period=1&type=1&uid=30175&variety=on&mode=print.html')
      .then(resp => resp.text())
      .then(function (filedata) {
        var coins = UCoinHtmlDataProvider._parseCoinsRegular(filedata, 'germany', '1', CoinType.CIRCULAR);
        expect(coins.length).toEqual(447); // 650 ?

        let coin73: Coin = {
          "id": "10316185_0_0",
          "cid": "10316185",
          "vid": null,
          "tid": null,

          "country":"germany",
          "period":"1",
          "type": CoinType.CIRCULAR,

          "value": "1c",
          "year": "2015",
          "variety": '1c (F)',
          "km": "KM# 207",
          "imgpath": "https://i.ucoin.net/coin/3/075",
          "imgsample": "3075762",
          "imgcode": "germany-1-cent-2015",

          "userOwn" : true,

          "image1" : 'https://i.ucoin.net/coin/3/075/3075762-1c/germany-1-cent-2015.jpg',
          "image2" : 'https://i.ucoin.net/coin/3/075/3075762-2c/germany-1-cent-2015.jpg',
          "details": '/coin/germany-1-cent-2015/?ucid=10316185'
        };

        expect(coins[73]).toEqual(coin73);
      });
  });

  it('(commemorative) _parseCoinsCollector #1', () => {
    // return fetch('/base/test-resources/table__country=germany&period=1&type=2&variety=on&mode=print.html') &uid=30175
    return fetch('/base/test-resources/table__country=germany&period=1&type=2&uid=30175&variety=on&mode=print.html')
      .then(resp => resp.text())
      .then(function (filedata) {
        var coins = UCoinHtmlDataProvider._parseCoinsCollector(filedata, 'germany', '1', CoinType.COMMEMORATIVE);
        expect(coins.length).toEqual(100);

        let coin10: Coin = {
          "id": "68030_21077_0",
          "cid": "68030",
          "vid": "21077",
          "tid": null,

          "country":"germany",
          "period":"1",
          "type": CoinType.COMMEMORATIVE,

          "value": '€2 (A)',
          "year": "2017",
          "variety": '€2 (A)',
          "name": "Bundeslander series - Porta Nigra, Rhineland-Palatinate",
          "km": "UC# 100",
          "imgpath": "https://i.ucoin.net/coin/12/608",
          "imgsample": "12608395",
          "imgcode": "germany-2-euro-2017",

          "userOwn" : false,

          "image1" : 'https://i.ucoin.net/coin/12/608/12608395-1c/germany-2-euro-2017.jpg',
          "image2" : 'https://i.ucoin.net/coin/12/608/12608395-2c/germany-2-euro-2017.jpg',
          "details": '/coin/germany-2-euro-2017/?cid=68030&vid=21077'
        };
        expect(coins[10]).toEqual(coin10);



        let coin17: Coin = {
          "id": "53945_16697_0",
          "cid": "53945",
          "vid": "16697",
          "tid": null,

          "country":"germany",
          "period":"1",
          "type": CoinType.COMMEMORATIVE,

          "value": "€2 (F)",
          "year": "2016",
          "variety": "€2 (F)",
          "name": "Bundeslander series - Zwinger, Saxony",
          "km": "KM# 347",
          "imgpath": "https://i.ucoin.net/coin/6/696",
          "imgsample": "6696084",
          "imgcode": "germany-2-euro-2016",

          "userOwn" : false,

          "image1" : 'https://i.ucoin.net/coin/6/696/6696084-1c/germany-2-euro-2016.jpg',
          "image2" : 'https://i.ucoin.net/coin/6/696/6696084-2c/germany-2-euro-2016.jpg',
          "details": '/coin/germany-2-euro-2016/?cid=53945&vid=16697'
        };

        expect(coins[17]).toEqual(coin17);
      });
  });


  // view-source:https://en.ucoin.net/coin/lithuania-1-cent-2015-2018/?tid=40558
  it('_parseCoinDetails', () => {
    return fetch('/base/test-resources/coin_lithuania-1-cent-2015-2018__tid=40558.html')
    // return fetch('/base/test-resources/table__country=germany&period=1&type=1&variety=on.html')
      .then(resp => resp.text())
      .then(function (filedata) {
        var coinDetails = UCoinHtmlDataProvider._parseCoinDetails(filedata, '0_0_40558');

        let coin0: CoinDetails = {
          "id": "0_0_40558",
          "image1big" : 'https://i.ucoin.net/coin/7/139/7139012-1s/lithuania-1-cent-2015.jpg',
          "image2big" : 'https://i.ucoin.net/coin/7/139/7139012-2s/lithuania-1-cent-2015.jpg',
          "price" : "$ 0.02"

        };
        expect(coinDetails).toEqual(coin0);
      });
  });



  // view-source:https://en.ucoin.net/users POST "evheny1985"
  it('_parseUserList', () => {
    return fetch('/base/test-resources/POST_users.html')
    // return fetch('/base/test-resources/table__country=germany&period=1&type=1&variety=on.html')
      .then(resp => resp.text())
      .then(function (filedata) {
        var users = UCoinHtmlDataProvider._parseUserList(filedata);

        let user0: User = {
          "id": "96829",
          "name": "Ana Rosa",
          "avatar": null,
          "country": 'Portugal',
          "coinsCount": 7,
          "swapCount": 10,
        };
        expect(users[0]).toEqual(user0);
      });
  });


});


