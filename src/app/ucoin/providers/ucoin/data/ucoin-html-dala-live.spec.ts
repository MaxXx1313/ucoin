
import { TestBed, inject } from '@angular/core/testing';

import { Http, HttpModule } from '@angular/http';
import { UCoinHtmlDataProvider } from './ucoin-html-data.service';
import { HttpCustom } from "../../api/http-custom.service";

jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

// Straight Jasmine testing without Angular's testing support
// https://stackoverflow.com/a/42047456
describe('ucoin-html-data-live', () => {

  let service: UCoinHtmlDataProvider;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
      ],
      providers: [
        UCoinHtmlDataProvider,
        HttpCustom,
      ]
    });
  });

  beforeEach(inject([UCoinHtmlDataProvider], (svc: UCoinHtmlDataProvider) => {
    service = svc;
  }));

  it('#findUser should run', (done) => {
    service.findUser('evheny1985').subscribe(searchResult => {
      expect(searchResult.items).toEqual(jasmine.any(Array));

      const firstUser = searchResult.items[0];

      expect(firstUser.id).toEqual('30175');
      expect(firstUser.name).toEqual('evheny1985');
      expect(firstUser.avatar).toEqual(null);
      expect(firstUser.country).toContain('Belarus');
      expect(firstUser.coinsCount).toEqual(jasmine.any(Number));
      expect(firstUser.swapCount).toEqual(jasmine.any(Number));

      // let user0 = {
      //   "id": "uid30175",
      //   "name": "evheny1985",
      //   "avatar": null,
      //   "country": null,
      //   "coinsCount": null,
      //   "swapCount": null,
      // };
      // expect(firstUser).toEqual(user0);
      done();

    });
  });


}); // -

