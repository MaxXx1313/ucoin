import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Country } from '../../../models/country';
import { Coin, CoinDetails, CoinType, CoinPeriod } from '../../../models/coin';
import { User } from '../../../models/user';
import { SearchResult } from '../../../models/SearchResult';
import { HttpCustom } from '../../api/http-custom.service';
import jq from "jqlite";
import { Log } from "../../../../../decorators/Log";
import { substr_regex } from "../../../../../utils";
import { Observable } from "rxjs";
import { LogObservable } from "../../../../../decorators/LogObservable"; // we have native $ supported in chrome. it puts some challenge on debug

@Injectable()
export class UCoinHtmlDataProvider {
  private baseUrl: string = 'https://en.ucoin.net/'; // TODO: move to config

  /**
   * @param {HttpCustom} request
   */
  constructor(
    public request: HttpCustom,
  ) {
  }

  /**
   * Get country list from ucoin
   * @returns {Observable<Array<Country>>}
   */
  @LogObservable()
  countries(): Observable<Array<Country>> {
    return this.request.get(this.baseUrl + '/catalog')
      .map(response => UCoinHtmlDataProvider._parseCatalogCountries(response.text()));
  }

  /**
   * Get country periods list from ucoin
   * @param {Country} country
   * @returns {Observable<Array<CoinPeriod>>}
   */
  @LogObservable()
  periods(country: Country): Observable<Array<CoinPeriod>> {
    return this.request.get(this.baseUrl + '/models/catalog_tree.plx?lid=1')
      .map(response => UCoinHtmlDataProvider._parseCoinPeriods(response.text(), country.id));
  }

  /**
   * Get coins list from ucoin
   * @param {CoinPeriod} period - period
   * @param {CoinType} type - coin type
   * @param {string} [userId] - user id
   * @returns {Observable<Array<Coin>>}
   */
  @LogObservable()
  coins(period: CoinPeriod, type: CoinType, userId?: string): Observable<Array<Coin>> {
    return this.request.get(this.baseUrl + '/table/?country=' + period.country + '&period=' + period.id + '&type=' + type + '&mode=print&variety=on' + (userId ? '&uid=' + userId : ''))
      .map(response => {
        switch (type) {
          case CoinType.CIRCULAR:
            return UCoinHtmlDataProvider._parseCoinsRegular(response.text(), period.country, period.id, type);

          case CoinType.COMMEMORATIVE:
          case CoinType.COLLECTOR:
            return UCoinHtmlDataProvider._parseCoinsCollector(response.text(), period.country, period.id, type);
          default:
            throw new Error('Unknown type: ' + type);
        }
      });
  }


  /**
   * Get coins list from ucoin
   * @param {Coin} coin
   * @returns {Observable<Array<Coin>>}
   */
  @LogObservable()
  coinDetails(coin: Coin): Observable<CoinDetails> {
    if (!coin.details) {
      return Observable.of(UCoinHtmlDataProvider._noCoinDetails(coin));
    }

    return this.request.get(this.baseUrl + coin.details)
      .map(response => UCoinHtmlDataProvider._parseCoinDetails(response.text(), coin.id));
  }

  /**
   * @param {string} url
   * @return {Observable<ArrayBuffer>}
   */
  @LogObservable()
  downloadImage(url: string): Observable<ArrayBuffer> {
    return this.request.get(url).map(response => response.arrayBuffer());
    // return Observable.fromPromise(fetch(url)
    //   .then((response: Response) => response.arrayBuffer())
    // );
  }



  /**
   * Find user
   * @param string
   * @returns {Observable<User[]>}
   */
  @LogObservable()
  findUser(searchString: string): Observable<SearchResult<User>> {
    const formData = {
     'search': searchString,
     'country': '',
    };
    return this.request.request('POST', this.baseUrl + '/users', UCoinHtmlDataProvider.serializeFormData(formData), {'Content-Type': 'application/x-www-form-urlencoded'})
      .map(response => UCoinHtmlDataProvider._parseUserList(response.text()))
      .map(users => {
        return {items: users} as SearchResult<User>;
      });
  }





  /**
   *
   */
  static prepareHtml(responseText: string): string {
      // remove link tags
      responseText = responseText.replace(/<link (.*?)>/ig, '');

      // remove scripts
      responseText = responseText.replace(/<script(.*?)[^\/]>(.*?)<\/script>/ig, '');
      responseText = responseText.replace(/<script(.*?)>/ig, '');
      // replace image tag with 'noimg' tag, so brouser doesn't fetch image from 'src' attribute
      responseText = responseText.replace(/<img(.*?)>/ig, '<noimg $1></noimg>');
      return responseText;
  }

  /**
   *
   */
  static serializeFormData(formData: any): any {
    let s = '';
    for(let prop in formData) {
       s += '&' + encodeURIComponent(prop) +'=' + encodeURIComponent(formData[prop]);
    }
    return s.substr(1);
  }


  /**
   * grab country list from arbitrary htlm response
   */
  static _parseCatalogCountries(responseText: string): Array<Country> {
    responseText = UCoinHtmlDataProvider.prepareHtml(responseText);

    var countryList: Array<Country> = [];

    jq(responseText).find('#catalog-index .cntry a:first-child').each(function (i, item) {
      const $item = jq(item);

      var url = $item.attr('href');
      var id = (url.match(/country=([\w]+)/) || [])[1];
      var name = $item.find('.left').last().text();
      var coinsCount = parseInt($item.find('.right').first().text());

      countryList.push({
        id: id,
        name: name,
        coinsCount: coinsCount
      });
    });
    return countryList;
  }


  /**
   * grab coin periods from arbitrary htlm response
   */
  static _parseCoinPeriods(responseText: string, countryId: string): Array<CoinPeriod> {
    responseText = UCoinHtmlDataProvider.prepareHtml(responseText);

    var periodList: Array<CoinPeriod> = [];

    jq(responseText).find('.sprite-' + countryId).first().parent().next('.periods').find('.period').each(function (i, item) {
      const $item = jq(item);

      // debugger;
      var url = $item.attr('href');
      var id: string = (url.match(/period=([\d]+)/) || [])[1];
      var content: Array<string> = Array.prototype.slice.call(item.childNodes).map(e => e.innerText || e.nodeValue || $(e).text());
      var name: string = content[0];
      var dates: string = content[1];

      periodList.push({
        id: id,
        country: countryId,
        name: name,
        dates: dates
      });
    });
    return periodList;
  }


  /**
   * grab coins data from arbitrary html response
   * For regular coins
   */
  static _parseCoinsRegular(responseText: string, countryId: string, periodId: string, type: CoinType): Array<Coin> {
    responseText = UCoinHtmlDataProvider.prepareHtml(responseText);

    const coins: Array<Coin> = [];

    // jq('.table thead th') .size() ~= 11;
    jq(responseText).find('.table .cell').each(function (i, item) {
      const $item = jq(item);

      const coin = {
        country: countryId,
        period: periodId,
        type: type,
      } as Coin;

      // var wasUsedRegulary = jq(item).hasClass('marked-s');

      coin.details = $item.attr('href');

      coin.value = jq(UCoinHtmlDataProvider.getColumnHeader(item)).text();
      coin.value = UCoinHtmlDataProvider.myescape(coin.value);
      coin.year = jq(UCoinHtmlDataProvider.getRowHeader(item)).text();
      coin.variety = $item.text();
      coin.variety = UCoinHtmlDataProvider.myescape(coin.variety);

      coin.userOwn = $item.hasClass('marked-1');

      coin.km = item.dataset.tooltipKm;
      // if(hasImage)
      coin.imgpath = item.dataset.tooltipImgpath;
      coin.imgsample = item.dataset.tooltipSample;
      coin.imgcode = item.dataset.tooltipCode;

      UCoinHtmlDataProvider._processDerivatedFields(coin);
      if (!coin.id) {
        console.warn('Skip coin without ID:', coin);
        return;
      }
      coins.push(coin);
    });
    return coins;
  }

  /**
   * grab coins data from arbitrary htlm response.
   * For collector and commemorative coins
   */
  static _parseCoinsCollector(responseText: string, countryId: string, periodId: string, type: CoinType): Array<Coin> {
    responseText = UCoinHtmlDataProvider.prepareHtml(responseText);

    const coins: Array<Coin> = [];
    // jq('.table thead th') .size() == 4;
    jq(responseText).find('.table .cell').each(function (i, item) {
      const $item = jq(item);

      const coin = {
        country: countryId,
        period: periodId,
        type: type,
      } as Coin;

      coin.details = $item.attr('href');

      coin.value = $item.text(); // TODO: not actually correct. ex. "€2 (G)" for germany
      coin.value = UCoinHtmlDataProvider.myescape(coin.value);
      coin.year = jq(UCoinHtmlDataProvider.getRowHeader(item)).text();
      coin.variety = $item.text();
      coin.variety = UCoinHtmlDataProvider.myescape(coin.variety);
      coin.name = $item.closest('td').next().text();

      coin.userOwn = $item.hasClass('marked-1');

      coin.km = item.dataset.tooltipKm;
      // if(hasImage)
      coin.imgpath = item.dataset.tooltipImgpath;
      coin.imgsample = item.dataset.tooltipSample;
      coin.imgcode = item.dataset.tooltipCode;

      UCoinHtmlDataProvider._processDerivatedFields(coin);
      if (!coin.id) {
        console.warn('Skip coin without ID:', coin);
        return;
      }
      coins.push(coin);
    });

    return coins;
  }


  /**
   * derivated properties
   */
  static _processDerivatedFields(coin: any): any {

    coin.cid = substr_regex(coin.details, /cid=([\d]+)/, 1);
    coin.vid = substr_regex(coin.details, /vid=([\d]+)/, 1);
    coin.tid = substr_regex(coin.details, /tid=([\d]+)/, 1);

    // we need either cid+vid or tid to be set
    // if (coin.tid || (coin.cid && coin.vid) ) {

    // relax the rule, any on the Xid
    if (coin.tid || coin.cid || coin.vid) {
      coin.id = (coin.cid || '0') + '_' + ( coin.vid || '0') + '_' + ( coin.tid || '0');
    }

    if (!coin.id && coin.imgsample && coin.imgcode ) {
      coin.id = coin.imgsample + '_' + coin.imgcode;
    }

    coin.image1 = coin.imgsample ? coin.imgpath + '/' + coin.imgsample + '-1c' + '/' + coin.imgcode + '.jpg' : null;
    coin.image2 = coin.imgsample ? coin.imgpath + '/' + coin.imgsample + '-2c' + '/' + coin.imgcode + '.jpg' : null;
  }


  /**
   * grab extended coin data from arbitrary htlm response.
   */
  static _parseCoinDetails(responseText: string, coinId: string): CoinDetails {
    responseText = UCoinHtmlDataProvider.prepareHtml(responseText);

    const $html = jq(responseText);
    const imageTable = $html.find('.coin-img noimg');

    const coin = {} as CoinDetails;

    coin.id = coinId;

    coin.image1big = jq(imageTable.get(0)).attr('src');
    coin.image2big = jq(imageTable.get(1)).attr('src');

    coin.price = $html.find('.pricewj').text().replace(/^.*:/, '').trim();

    return coin;
  }


  /**
   *  Set CoinDetails when no link
   */
  static _noCoinDetails(coin: Coin): CoinDetails {
    console.log('UcoinData: _noCoinDetails for coin ', coin);
    const coinDetails = {} as CoinDetails;

    coinDetails.id = coin.id;

    coinDetails.image1big = coin.image1;
    coinDetails.image2big = coin.image2;

    coinDetails.price = null;

    return coinDetails;
  }

  /**
   * grab user list from arbitrary htlm response
   */
  static _parseUserList(responseText: string): Array<User> {
    responseText = UCoinHtmlDataProvider.prepareHtml(responseText);

    var users: Array<User> = [];

    const noAvatar = 'no-avatar.jpg'

    jq(responseText).find('#users .users-list tr').each(function (i, item) {
      const $item = jq(item);

      var avatar = $item.find('.avatar noimg').attr('src');
      if(avatar.endsWith(noAvatar)){
        avatar = null;
      }
      var id = $item.find('.avatar a').attr('href').replace('/', '').replace('uid', '');
      var name = $item.find('.avatar noimg').attr('alt');

      var userAttributes = $item.find('.username>*>*');
      var country = userAttributes.get(userAttributes.length - 2).innerText;
      var coinsCount = parseInt( $item.find('td').get(3).innerText.replace(/,/g, ''), 10);
      var swapCount = parseInt( $item.find('td').get(4).innerText.replace(/,/g, ''), 10);

      users.push({
        id: id,
        name: name,
        avatar: avatar,
        country: country,
        coinsCount: coinsCount,
        swapCount: swapCount
      });
    });
    return users;
  }


  /**
   *
   */
  static getColumnHeader(cell) {
    const jqtable = jq(cell).closest('table');
    const td = jq(cell).closest('td')[0];
    const icolumn = Array.prototype.indexOf.call(td.parentNode.childNodes, td);

    return jqtable.find('thead th').get(icolumn);
  }


  /**
   *
   */
  static getRowHeader(cell) {
    const td = jq(cell).closest('td')[0];
    const row = td.parentNode.childNodes[0];

    return row;
  }

  /**
   *
   */
  private static myescape(s: string): string {
    return decodeURIComponent(encodeURIComponent(s).replace('%C2%A0', '%20'));
  }


} // -
