import { Injectable } from '@angular/core';

import 'rxjs/add/operator/toPromise'; // fucking shit
import { Http, Headers, Response } from '@angular/http';
import { Observable } from "rxjs";

const PROXY_URL = window.location.protocol+'//'+window.location.host.replace(/(\:\d+)?$/, ':3128')+'/proxy';
const PROXY_HEADER = 'X-Request-To';


/**
 *
 */
@Injectable()
export class HttpCustom {

  public use_proxy: boolean = false;

  /**
   *
   */
  constructor(
    private http: Http,
    // private alertCtrl: AlertController
  ) {
    // this._token = this._loadToken();
    this.use_proxy = !window['cordova'] || window['cordova'].platformId == "browser";
    if (this.use_proxy) {
      console.warn('Browser detected! We use proxy for cors requests');
    }
  }

  /**
   * @param {'get'|'post'|'put'|'delete'} method
   * @param {string} url - method name
   * @param {any} data - data (post|put only)
   */
  public request(method: string, url: string, data?: any, headers?: any): Observable<any> {
    return ((this.use_proxy)
        ? this.request_proxy(method, url, data, headers)
        : this.request_direct(method, url, data, headers)
    );
  }

  /**
   *
   */
  private request_direct(method: string, url: string, data?: any, headers: any = {}): Observable<Response> {
    // return this.http.get(url/*, {cache:false}*/)
    return this.http.request(url, { method: method, body: data, headers: new Headers(headers) });
  }

  /**
   *
   */
  // for develop only
  private request_proxy(method: string, url, data?: any, headers: any = {}): Observable<Response> {
    headers = headers || {};
    if(headers[PROXY_HEADER]) {
      throw new Error(`Header ${PROXY_HEADER} is used for proxy`);
    }
    headers[PROXY_HEADER] = url;

    // "helperName" helps to find request in network inspector
    // actually, doesn't change the logic
    const helperName = url.replace(/^\w+:\/+.+?\//, '');

    return this.http.request(PROXY_URL + '?' + helperName, {
      method: method,
      body: data,
      headers: new Headers(headers)
    });
  }


  get(url: string): Observable<any> {
    return this.request('GET', url);
  }

  post(url: string, body: any) {
    return this.request('POST', url, body);
  }

  put(url: string, body: any) {
    return this.request('PUT', url, body);
  }

  delete(url: string) {
    return this.request('DELETE', url);
  }

  patch(url: string, body: any) {
    return this.request('PATCH', url, body);
  }

} // -


