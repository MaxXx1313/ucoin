import { Injectable } from '@angular/core';
import { ConnectionBackend, Http, RequestOptions } from '@angular/http';



@Injectable()
export class HttpInterceptor extends Http {

  constructor(
    backend: ConnectionBackend,
    defaultOptions: RequestOptions,
  ) {
    super(backend, defaultOptions);
  }

  /*
  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    return super.request(url, options); // Here is the error
  }
*/

} // -
