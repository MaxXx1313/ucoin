

import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeStyle } from "@angular/platform-browser";

@Component({
  selector: 'img-fit',
  template: '<div [style.background-image]="getLink()"></div>'
})
export class ImgFitComponent {
  @Input() src: string = '';

  /**
   * @return {string}
   */
  constructor(
    private sanitization: DomSanitizer
  ){}

  /**
   * @return {string}
   */
  getLink(): SafeStyle {
    return this.sanitization.bypassSecurityTrustStyle(`url("${this.src}")`);
  }
} // -





