import { Component, OnDestroy, OnInit } from '@angular/core';
import { CacheProgressEvent, UCoinCacheManager } from "../../providers/ucoin/ucoin-cache.service";



@Component({
  selector: 'cache-progress',
  template: `
    <div class="cache-progress-panel" *ngIf="cacheEvent && cacheEvent.active">
      <p *ngIf="!cacheEvent.total">{{cacheEvent.message}}</p>
      <p *ngIf="cacheEvent.total">Caching {{cacheEvent.current}} of {{cacheEvent.total}}</p>
    </div>
  `
  /*
  `
    <div class="cache-progress-panel">
      <p>Caching 123 of 456</p>
    </div>
    `
   */
})
export class CacheProgressComponent implements OnInit, OnDestroy {

  public cacheEvent: CacheProgressEvent;

  private _cacheProgressSubscription;

  /**
   *
   */
  constructor(
    private ucoinCache: UCoinCacheManager,
  ) {
  }


  /**
   *
   */
  ngOnInit() {
    this._cacheProgressSubscription = this.ucoinCache.cacheProgress.subscribe(this.onCacheProgress.bind(this));
  }

  /**
   *
   */
  ngOnDestroy() {
    this._cacheProgressSubscription.unsubscribe();
  }


  // TODO: move to nested component
  protected onCacheProgress(cacheEvent: CacheProgressEvent): void {
    this.cacheEvent = cacheEvent;
  }

}

