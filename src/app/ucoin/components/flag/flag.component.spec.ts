import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';

import { FlagComponent } from './flag.component';

describe('FlagComponent', () => {

  let component:    FlagComponent;
  let fixture: ComponentFixture<FlagComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ FlagComponent ], // declare the test component
    });

    fixture = TestBed.createComponent(FlagComponent);
    component = fixture.componentInstance; // FlagComponent test instance
  });
  

  it('no content', () => {

    // query for the title <h1> by CSS element selector
    let de:DebugElement = fixture.debugElement.query(By.css('span'));
    let el:HTMLElement = de.nativeElement;

    expect(el.textContent).toEqual('');
  });

  it('should has valid class', () => {
    component.country = 'russia';
    fixture.detectChanges();


    //check class
    let de:DebugElement = fixture.debugElement.query(By.css('sprite-russia'));
    expect(de).toBeDefined();
    
    //check class
    let de1:DebugElement = fixture.debugElement.query(By.css('sprite-belarus'));
    expect(de1).toBeNull();

  });


});


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/