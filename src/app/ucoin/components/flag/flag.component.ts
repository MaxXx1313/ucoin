import { Component, Input } from '@angular/core';

@Component({
  selector: 'flag',
  template: '<span class="flag sprite-{{country}}"></span>'
})
export class FlagComponent {
  @Input() country:string = '';
}

