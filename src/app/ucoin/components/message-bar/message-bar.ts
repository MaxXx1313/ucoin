

import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeStyle } from "@angular/platform-browser";

@Component({
  selector: 'message-bar',
  template: `
    <div class="ms-MessageBar ms-MessageBar-{{color}}">
      <div class="ms-MessageBar-content">
        <div class="ms-MessageBar-icon">
          <ion-icon [name]="icon"></ion-icon>
        </div>
        <div class="ms-MessageBar-text">
          <ng-content></ng-content>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .ms-MessageBar {
      padding: 15px;  
    }
    .ms-MessageBar-warning {
      background-color: #fff4ce;
      color: #333;
    }
    
    .ms-MessageBar-content{
      display: flex;
    }
    .ms-MessageBar-icon {
      margin: 0 5px;
    }
  `]
})
export class MessageBarComponent {
  @Input() color: string = 'warning';
  @Input() icon: string = 'warning';

  /**
   * @return {string}
   */
  constructor(
  ){}

} // -





