import { ModuleWithProviders, NgModule } from '@angular/core';

import { CommonModule } from "@angular/common";
import { CacheProgressComponent } from "./components/cacheProgress/cache-progress.component";
import { FlagComponent } from "./components/flag/flag.component";
import { ImgFitComponent } from "./components/img-fit/img-fit.component";
import { MapToIterablePipe } from "./pipes/mapToIterable";
import { MessageBarComponent } from "./components/message-bar/message-bar";
import { IonicModule } from "ionic-angular";
import { UCoinData } from './providers/ucoin/ucoin-data.service';
import { UCoinCacheManager } from './providers/ucoin/ucoin-cache.service';
import { UCoinHtmlDataProvider } from './providers/ucoin/data/ucoin-html-data.service';
import { UCoinLocalDataProvider } from './providers/ucoin/data/ucoin-local-data.service';



const sharedExports = [
  CacheProgressComponent,
  FlagComponent,
  ImgFitComponent,
  MapToIterablePipe,
  MessageBarComponent,
];


@NgModule({
  declarations: sharedExports,
  imports: [
    CommonModule,
    IonicModule,
  ],
  exports: sharedExports
})
export class UcoinModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: UcoinModule,
      providers: [
        UCoinData,
        UCoinCacheManager,
        UCoinHtmlDataProvider,
        UCoinLocalDataProvider,
      ]
    };
  }
}
