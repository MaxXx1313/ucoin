import { OfflineData } from "./offlineData";

/**
   {
    "id": "31220_10896",
    "cid": 31220,
    "vid": 10896,
    "value": "$10",
    "year": "1985",
    "variety": "$10",
    "name": "Royal Visit of Queen Elizabeth II",
    "selected": false,
    "km": "KM# 5",
    "imgpath": "https://i.ucoin.net/coin/17/361",
    "imgsample": "17361916",
    "imgcode": "antigua_barbuda-10-dollars-1985",
    "details": "/coin/antigua_barbuda-10-dollars-1985/?tid=73840"
  },
 */
export interface Coin extends OfflineData {

  /**
   * Coun unique ID.
   * Technically, it's: cid + '_' + vid
   * @example "31220_10896"
   */
  id: string;

  /**
   * Country ID
   */
  country: string;

  /**
   * Period ID
   */
  period: string;

  /**
   * Period ID
   */
  type: CoinType;

  /**
   * Coin specimen ID
   * Different variety can have the same specimen. See german coins for an example
   * @example 31220
   */
  cid?: string;

  /**
   * Coin variety ID
   * @example 10896
   */
  vid?: string;

  /**
   * Coin ??? ID
   * @example 42263
   */
  tid?: string;

  /**
   * human readable coin value
   * @example "$10"
   * @example "€2 (G)"
   */
  value: string;

  /**
   * coin year
   * @example "1985"
   */
  year: string;

  /**
   * @example "$10"
   * @example "€2 (G)"
   */
  variety: string;

  /**
   * @example "25th Anniversary - German Unification"
   */
  name?: string; // for named coins only

  /**
   * @example "UC# 101"
   */
  km: string; // krause



  /**
   * @see {@link image1} to see it's usage
   * @example "https://i.ucoin.net/coin/1/858"
   */
  imgpath: string;

  /**
   * @see {@link image1} to see it's usage
   * @example "1858898"
   */
  imgsample: string;

  /**
   * @see {@link image1} to see it's usage
   * @example "germany-2-euro-2014"
   */
  imgcode: string;


  /**
   * link to coin details
   * @example "/coin/germany-2-euro-2014/?cid=31220&vid=10896"
   */
  details: string;


  // <img src="https://i.ucoin.net/coin/3/693/3693533-1c/spain-5-cents-2014.jpg" style="width:120px;height:120px;">
  // '-1s' for big images?
  /**
   * Coin image url
   * technically, it's: this.imgpath + '/' + this.imgsample + '-1c' + '/' + this.imgcode + '.jpg';
   */
  image1?: string;

  /**
   * Coin image url
   * technically, it's: this.imgpath + '/' + this.imgsample + '-2c' + '/' + this.imgcode + '.jpg';
   */
  image2?: string;


  /**
   * Define whether a user owns the coin
   */
  userOwn: boolean;


  /**
   * Amount of coins for the country
   */
  coinsCount?: number;

} // -

/**
 * {id: "743", name: "Доллар", dates: "1965 - 2018"}
 */
export interface CoinPeriod extends OfflineData {

  /**
   * period ID
   * @example "743"
   */
  id: string;

  /**
   * Country ID
   */
  country: string;

  /**
   * period name
   * @example "Доллар"
   * @example "East Germany (DDR)"
   */
  name: string;

  /**
   * period dates
   * @example "1965 - 2018"
   */
  dates: string;


  cachePolicy?: {
    regular?: boolean;
    commemorative?: boolean;
    collector?: boolean;
  };

} // -


/**
 *
 */
export enum CoinType {
  CIRCULAR = 1,
  COMMEMORATIVE = 2,
  COLLECTOR = 3
}



export interface CoinDetails extends OfflineData {
  /**
   * Coin ID
   */
  id: string;

  /**
   * image 1 url
   * @example https://i.ucoin.net/coin/7/139/7139012-1s/lithuania-1-cent-2015.jpg
   */
  image1big: string;
  /**
   * image 2 url
   * @example https://i.ucoin.net/coin/7/139/7139012-2s/lithuania-1-cent-2015.jpg
   */
  image2big: string;

  /**
   * minimal value
   * This is an arbitrary string so far
   * @example " $ 0.02"
   */
  price: string;
  // TODO: add extra details
}
