
export type UserId = string;

/**
 * https://en.ucoin.net/uid30175
 */
export interface User {

  /**
   * user ID
   * Also used in uid calculation: "uid30175"
   * @example "30175"
   */
  id: UserId;
  name: string;
  avatar: string;
  // hasPro: boolean;

  country: string; // country name
  coinsCount: number;
  swapCount: number;

} // -
