/**
 *
 */
import { OfflineData } from "./offlineData";

export interface Country extends OfflineData {

  /**
   * country ID
   * @example "antigua_barbuda"
   */
  id: string;

  /**
   * Country name
   * @example "Antigua and Barbuda"
   */
  name: string;

  /**
   *
   */
  coinsCount?: Number;

} // -
