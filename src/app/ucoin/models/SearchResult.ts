/**
 */
export interface SearchResult<T> {

  items: Array<T>;

  prevCursor?: string;
  nextCursor?: string;

} // -