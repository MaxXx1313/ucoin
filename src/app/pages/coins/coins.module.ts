import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { CoinsPage } from './coins';
import { UcoinModule } from "../../ucoin/ucoin.module";


@NgModule({
  declarations: [
    CoinsPage,
  ],
  imports: [
    UcoinModule,
    IonicPageModule.forChild(CoinsPage),
    TranslateModule.forChild()
  ],
  exports: [
    CoinsPage
  ]
})
export class CoinsPageModule { }
