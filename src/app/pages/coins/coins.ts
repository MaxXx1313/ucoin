import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Country } from '../../ucoin/models/country';
import { Coin, CoinPeriod, CoinType } from '../../ucoin/models/coin';
import { MapToIterablePipe } from "../../ucoin/pipes/mapToIterable";
import { UCoinData } from "../../ucoin/providers/ucoin/ucoin-data.service";
import { Subscription } from "rxjs";
import { Reducer } from "../../../utils";

// coins, grouped by year
interface CoinsGrouped {
  key: string; // year
  value: Array<Coin>;
}

interface CoinsArrayData<T> {
  1: Array<T>; // regular
  2: Array<T>; // commemorative
  3: Array<T>; // collector
}

@IonicPage()
@Component({
  selector: 'coins',
  templateUrl: 'coins.html'
})
export class CoinsPage implements OnInit {

  public loading: boolean;
  public country: Country;
  public period: CoinPeriod;
  public coinTab: string;
  public coins: CoinsArrayData<Coin> = {1: [], 2: [], 3: []};
  public coinsGrouped: CoinsArrayData<CoinsGrouped> = {1: [], 2: [], 3: []};
  public coinsLoaded = {1: false, 2: false, 3: false};
  protected mapToIterable: MapToIterablePipe = new MapToIterablePipe();

  /**
   *
   */
  constructor(
    private navParams: NavParams,
    private navCtrl: NavController,
    private backend: UCoinData,
  ) {
    this.country = this.navParams.get('country');
    this.period = this.navParams.get('period');
    this.coinTab = '1';
  }

  /**
   *
   */
  ngOnInit() {
    if (!this.country || !this.period) {
      this.navCtrl.setRoot('CountriesPage');
      return;
    }

    this.loadCoins(this.coinTab);
  }

  /**
   *
   */
  loadCoins(tabIndex: string): Subscription {
    return this._loadCoins(parseInt(tabIndex) as CoinType);
  }

  /**
   *
   */
  _loadCoins(type: CoinType): Subscription {
    if (this.coinsLoaded[type]) {
      return;
    }

    this.loading = true;
    return this.backend.coins(this.period, type)
      .subscribe(data => {
        this.coins[type] = data;

        // group by year
        this.coinsGrouped[type] = this.mapToIterable.transform(this.coins[type].reduce(Reducer.groupByKey('year'), {})).reverse();

        this.coinsLoaded[type] = true;

        //
        this.loading = false;
      }, e => {
        this.loading = false;
        throw e;
      });
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(coin: Coin) {
    this.navCtrl.push('CoinDetailPage', {coin: coin});
  }


} // -
