import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { CoinDetailPage } from './coin-detail';
import { UcoinModule } from "../../ucoin/ucoin.module";

@NgModule({
  declarations: [
    CoinDetailPage,
  ],
  imports: [
    UcoinModule,
    IonicPageModule.forChild(CoinDetailPage),
    TranslateModule.forChild()
  ],
  exports: [
    CoinDetailPage
  ]
})
export class CoinDetailPageModule { }
