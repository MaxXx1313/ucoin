import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Coin, CoinDetails } from "../../ucoin/models/coin";
import { UCoinData } from "../../ucoin/providers/ucoin/ucoin-data.service";
import { Subscription } from "rxjs";



@IonicPage()
@Component({
  selector: 'page-coin-detail',
  templateUrl: 'coin-detail.html'
})
export class CoinDetailPage implements OnInit {

  public loading: boolean;
  public coin: Coin;
  public coinDetails: CoinDetails;


  /**
   *
   */
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private backend: UCoinData,
  ) {
    this.coin = navParams.get('coin');
  }

  /**
   * @returns {any}
   */
  ngOnInit() {
    if (!this.coin) {
      this.navCtrl.setRoot('CountriesPage');
      return;
    }

    return this.reload();
  }

  /**
   *
   */
  reload(): Subscription {
    this.loading = true;
    return this.backend.coinDetails(this.coin)
      .subscribe(data => {
        this.coinDetails = data;
        this.loading = false;
      }, e => {
        this.loading = false;
        throw e;
      });
  }


} // -
