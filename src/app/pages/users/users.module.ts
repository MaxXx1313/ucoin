import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { UsersPage } from "./users";
import { UcoinModule } from "../../ucoin/ucoin.module";


@NgModule({
  declarations: [
    UsersPage,
  ],
  imports: [
    UcoinModule,
    IonicPageModule.forChild(UsersPage),
    TranslateModule.forChild()
  ],
  exports: [
    UsersPage,
  ]
})
export class UsersPageModule { }
