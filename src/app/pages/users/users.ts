import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController } from 'ionic-angular';
import { UCoinData } from "../../ucoin/providers/ucoin/ucoin-data.service";
import { User } from "../../ucoin/models/user";
import { UserAddPage } from "../user-add/user-add";
import { IonPage } from "../ionPage";
import { Observable } from 'rxjs';
import { UCoinCacheManager } from '../../ucoin/providers/ucoin/ucoin-cache.service';



@IonicPage()
@Component({
  selector: 'page-users',
  templateUrl: 'users.html'
})
export class UsersPage implements IonPage {

  public followUsers$: Observable<User[]>;

  /**
   *
   */
  constructor(
    private navCtrl: NavController,
    private backend: UCoinData,
    public alertCtrl: AlertController,
    public cacheService: UCoinCacheManager,
  ) {
    this.followUsers$ = this.cacheService.getCacheUsers();
  }

  /**
   * Navigate to the detail page for this item.
   */
  removeUser(user: User): Promise<any> {
    return this.confirmRemoveUser(user.name).then(isConfirmed => {
      if (isConfirmed) {
        this.cacheService.removeCacheUser(user.id);
      }
    });
  }

  /**
   */
  addUser() {
    this.navCtrl.push('UserAddPage');
  }

  /**
   * @param name
   */
  protected confirmRemoveUser(name: string): Promise<boolean> {
    return new Promise((resolve) => {
      const alert = this.alertCtrl.create({
        title: 'Remove user?',
        subTitle: 'Remove the user "' + name + '"?',
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
              console.log('Disagree clicked');
              resolve(false);
            }
          },
          {
            text: 'Remove',
            cssClass: 'text-danger',
            handler: () => {
              console.log('Agree clicked');
              resolve(true);
            }
          }
        ]
      });
      alert.present();
    });
  }


} // -
