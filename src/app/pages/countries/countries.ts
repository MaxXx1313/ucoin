import { Component, OnInit } from '@angular/core';
import { ActionSheetController, IonicPage, NavController } from 'ionic-angular';

import { Country } from '../../ucoin/models/country';
import { MapToIterablePipe } from "../../ucoin/pipes/mapToIterable";
import { Coin } from "../../ucoin/models/coin";
import { UCoinData } from "../../ucoin/providers/ucoin/ucoin-data.service";

// coins, grouped by year
interface CountryGrouped {
  key: string; // first year
  value: Array<Coin>;
}

const MIN_SEARCH_CHARS = 2;

@IonicPage()
@Component({
  selector: 'page-countries',
  templateUrl: 'countries.html'
})
export class CountriesPage implements OnInit {

  public loading: boolean;
  public showAll: boolean;
  public showFavorites: boolean;
  public countries: Array<Country> = [];
  public countriesFavorite: Array<Country> = [];
  public countriesGroup: Array<CountryGrouped> = [];
  protected mapToIterable: MapToIterablePipe = new MapToIterablePipe();

  /**
   *
   */
  constructor(
    private navCtrl: NavController,
    private backend: UCoinData,
    public actionSheetCtrl: ActionSheetController,
  ) {
  }

  ngOnInit() {
    return this.reload();
  }


  getItems(ev: any) {
    // Reset items back to all of the items

    // set val to the value of the searchbar
    const searchString = (ev.target.value || '').trim();
    this._applyFilter(searchString);

  }

  /**
   *
   */
  reload() {
    this.loading = true;
    return this.backend.countries()
      .subscribe((countries) => {
        this.countries = countries;
        this.countriesFavorite = countries.filter(country => country.availableLocal);

        this._applyFilter();

        //
        this.loading = false;
      }, e => {
        this.loading = false;
        throw e;
      });
  }


  /**
   * @param searchStr
   * @private
   */
  protected _applyFilter(searchStr: string = '') {
    const searchStrLC = (searchStr || '').trim().toLowerCase();

    const applyFilter = searchStrLC.length >= MIN_SEARCH_CHARS;

    this.showAll = this.countriesFavorite.length == 0 || applyFilter;
    this.showFavorites = this.countriesFavorite.length > 0 && !applyFilter;

    let countries;
    // if the value is an empty string don't filter the items
    if (applyFilter) {
      countries = this.countries.filter((item) => {
        return (item.name.toLowerCase().indexOf(searchStrLC) > -1);
      });
    } else {
      countries = this.countries;
    }


    // group by first letter
    const countriesGroup = countries.reduce(function (group, country) {
      const key = country.name.substring(0, 1).toUpperCase();
      group[key] = group[key] || [];
      group[key].push(country);
      return group;
    }, {});


    this.countriesGroup = this.mapToIterable.transform(countriesGroup);
  }


  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Country) {
    this.navCtrl.push('CountryItemPage', {country: item});
  }


  /**
   * @param {Country} country
   */
  showOptions(country: Country) {
    const actionSheet = this.actionSheetCtrl.create({
      title: country.name,
      buttons: [
        {
          text: country.availableLocal ? 'Don\'t keep offline' : 'Keep offline',
          icon: country.availableLocal ? 'cloud' : 'cloud-download',
          handler: () => {
            // this.backend.setCountryCacheMode(country, !country.availableLocal);
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

} // -
