import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { CountriesPage } from './countries';
import { UcoinModule } from "../../ucoin/ucoin.module";


@NgModule({
  declarations: [
    CountriesPage
  ],
  imports: [
    UcoinModule,
    IonicPageModule.forChild(CountriesPage),
    TranslateModule.forChild()
  ],
  exports: [
    CountriesPage
  ]
})
export class ContentPageModule { }
