import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { UserAddPage } from "./user-add";
import { UcoinModule } from "../../ucoin/ucoin.module";


@NgModule({
  declarations: [
    UserAddPage,
  ],
  imports: [
    UcoinModule,
    IonicPageModule.forChild(UserAddPage),
    TranslateModule.forChild()
  ],
  exports: [
    UserAddPage,
  ]
})
export class UsersPageModule { }
