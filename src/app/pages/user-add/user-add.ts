import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { UCoinData } from "../../ucoin/providers/ucoin/ucoin-data.service";
import { User } from "../../ucoin/models/user";
import { SearchResult } from "../../ucoin/models/SearchResult";
import { UCoinCacheManager } from '../../ucoin/providers/ucoin/ucoin-cache.service';



const MIN_SEARCH_CHARS = 2;

@IonicPage()
@Component({
  selector: 'page-user-add',
  templateUrl: 'user-add.html'
})
export class UserAddPage implements OnInit {

  public loading: boolean;
  public users: SearchResult<User>;

  /**
   *
   */
  constructor(
    private navCtrl: NavController,
    private backend: UCoinData,
    public cacheService: UCoinCacheManager,
  ) {
  }

  ngOnInit() {
    // return this.reload();
  }


  /**
   *
   */
  find(searchString: string) {
    this.loading = true;
    return this.backend.findUser(searchString)
      .subscribe((result) => {
        this.users = result;
        this.loading = false;
      }, e => {
        this.loading = false;
        throw e;
      });
  }


  /**
   */
  addUser(user) {
    this.cacheService.addCacheUser(user);
    this.navCtrl.pop();
  }

  /**
   */
  getItems(ev) {
    const searchString = (ev.target.value || '').trim();
    if (searchString && searchString.length >= MIN_SEARCH_CHARS) {
      this.find(ev.target.value);
    } else {
      this.users = null;
    }
  }


} // -
