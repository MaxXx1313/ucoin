import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { CountryItemPage } from './country-item';
import { UcoinModule } from "../../ucoin/ucoin.module";

@NgModule({
  declarations: [
    CountryItemPage,
  ],
  imports: [
    UcoinModule,
    IonicPageModule.forChild(CountryItemPage),
    TranslateModule.forChild()
  ],
  exports: [
    CountryItemPage
  ]
})
export class CountryItemPageModule { }
