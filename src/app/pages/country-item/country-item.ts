import { Component, OnInit } from '@angular/core';
import { ActionSheetController, IonicPage, NavController, NavParams } from 'ionic-angular';

import { Country } from '../../ucoin/models/country';
import { CoinPeriod, CoinType } from '../../ucoin/models/coin';
import { UCoinData } from "../../ucoin/providers/ucoin/ucoin-data.service";
import { UCoinCacheManager } from "../../ucoin/providers/ucoin/ucoin-cache.service";



@IonicPage()
@Component({
  selector: 'country-item',
  templateUrl: 'country-item.html'
})
export class CountryItemPage implements OnInit {
  public loading: boolean;

  public country: Country;
  public periods: Array<CoinPeriod>;

  /**
   *
   */
  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private backend: UCoinData,
    public actionSheetCtrl: ActionSheetController,
    private cacheManager: UCoinCacheManager,
  ) {
  }

  ngOnInit() {
    return this.reload();
  }


  /**
   *
   */
  reload() {
    this.country = this.navParams.get('country');
    if (!this.country) {
      this.navCtrl.setRoot('CountriesPage');
      return;
    }

    //
    this.loading = true;
    return this.backend.periods(this.country)
      .subscribe(data => {
        this.periods = data;
        this.loading = false;
      }, e => {
        this.loading = false;
        throw e;
      });
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: CoinPeriod) {
    this.navCtrl.push('CoinsPage', {period: item, country: this.country});
  }


  /**
   * @param {CoinPeriod} period
   */
  showOptions(period: CoinPeriod) {
    const cachePolicy = period.cachePolicy || {};
    const actionSheet = this.actionSheetCtrl.create({
      title: period.name,
      buttons: [
        {
          text: cachePolicy.regular ? 'Don\'t keep circular offline' : 'Keep circular offline',
          icon: cachePolicy.regular ? 'trash' : 'cloud-download',
          handler: () => {
            this.cacheManager.setPeriodCacheMode(period, CoinType.CIRCULAR, !cachePolicy.regular);
            cachePolicy.regular = !cachePolicy.regular;
            period.availableLocal = cachePolicy.regular || cachePolicy.commemorative || cachePolicy.collector;
          }
        },
        {
          text: cachePolicy.commemorative ? 'Don\'t keep commemorative offline' : 'Keep commemorative offline',
          icon: cachePolicy.commemorative ? 'trash' : 'cloud-download',
          handler: () => {
            this.cacheManager.setPeriodCacheMode(period, CoinType.COMMEMORATIVE, !cachePolicy.commemorative);
            cachePolicy.commemorative = !cachePolicy.commemorative;
            period.availableLocal = cachePolicy.regular || cachePolicy.commemorative || cachePolicy.collector;
          }
        },
        {
          text: cachePolicy.collector ? 'Don\'t keep collector offline' : 'Keep collector offline',
          icon: cachePolicy.collector ? 'trash' : 'cloud-download',
          handler: () => {
            this.cacheManager.setPeriodCacheMode(period, CoinType.COLLECTOR, !cachePolicy.collector);
            cachePolicy.collector = !cachePolicy.collector;
            period.availableLocal = cachePolicy.regular || cachePolicy.commemorative || cachePolicy.collector;
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

} // -
