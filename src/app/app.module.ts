import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicApp, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { MyStorage } from "./ucoin/providers/storage/MyStorage";
import { MyErrorHandler } from "./ucoin/providers/error/MyErrorHandler";
import { fileProviderFactory, FileStorage } from "./ucoin/providers/fileStorage/fileProviderFactory";
import { Settings } from "./ucoin/providers/settings/settings";
import { HttpCustom } from "./ucoin/providers/api/http-custom.service";
import { UCoinData } from "./ucoin/providers/ucoin/ucoin-data.service";
import { UCoinCacheManager } from "./ucoin/providers/ucoin/ucoin-cache.service";
import { UcoinModule } from "./ucoin/ucoin.module";


// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

/**
 * Default setings
 */
export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    followUser: null,

    offlineMode: false,
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    UcoinModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    HttpCustom,
    MyStorage,
    fileProviderFactory,

    File,
    SplashScreen,
    StatusBar,
    {provide: Settings, useFactory: provideSettings, deps: [Storage]},

    {
      provide: APP_INITIALIZER,
      useFactory: init_providers,
      deps: [FileStorage, UCoinData, UCoinCacheManager, Settings],
      multi: true
    },

    // Keep this to enable Ionic's runtime error handling during development
    // { provide: ErrorHandler, useClass: IonicErrorHandler }
    {provide: ErrorHandler, useClass: MyErrorHandler}
  ]
})
export class AppModule {
}


/**
 *
 */
export function init_providers(file: FileStorage, ucoin: UCoinData, cache: UCoinCacheManager, settings: Settings) {
  return () => Promise.all([
    file.ready(),
    ucoin.ready(),
    settings.ready(),
  ]).then(() => Promise.all([
    cache.ready(),
  ]))
}
