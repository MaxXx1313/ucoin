import { Observable } from "rxjs";


window['Observable'] = Observable;

/**
 * @param {string} str
 * @param {RegExp} expression
 * @param {number} [matchIndex]
 * @returns {string}
 */
export function substr_regex(str: string, expression: RegExp, matchIndex: number = 0): string {
  const match = str ? str.match(expression) : null;
  return match ? match[matchIndex] : null;
}


/**
 * Create an observable which emit items "last over first", i.e. the last observable has a higher priority
 * @param {Observable<T>} observableArr
 * @return {Observable<T>}
 */
export function ObservablePriorityLast<T>(observableArr: Array<Observable<T>>): Observable<T> {
  let maxResolved = -1;
  let maxRejected = -1;
  let lastError = null;

  let completedCount = 0;
  let errorsCount = 0;
  return new Observable(subscribe => {

    for (let i = 0; i < observableArr.length; i++) {
      const deferred = observableArr[i].subscribe( (result: T) => {
        deferred && deferred.unsubscribe(); //HOTFIX: for some reason deferred can be undefined here
        if (maxResolved < i) {
          maxResolved = i;
          subscribe.next(result);
        } else {
          // skip, because observable with higher priority are resolved already
        }

      }, e => {
        if (maxRejected < i) {
          maxRejected = i;
          lastError = e;
        }

        //
        errorsCount++;
        if(errorsCount >= observableArr.length ){
          // all observers return an error
          subscribe.error(lastError);
        }
      }, () => {
        completedCount++;
        if (completedCount >= observableArr.length){
          subscribe.complete();
        }
      });
    }
  });
}





/**
 * extract object property value by point-divided selector like 'book.meta.author'
 * @return {*|null} value
 */
export function xpath_get(obj: any, path: (string|Array<string>), separator: string = '.'): any {
  let tokens = Array.isArray(path) ? path : path.split(separator);
  let result = obj;
  for( let i=0; result && (i<tokens.length); i++){
    result = result[tokens[i]];
  }
  return result;
}



/**
 * set object property value by point-divided selector like 'book.meta.author'
 * @return {object} updated object
 */
export function xpath_set<T extends any>(obj: T, path: (string|Array<string>), value: any, separator: string = '.'): T {
  let tokens = Array.isArray(path) ? path : path.split(separator);
  let result = obj;
  let i=0;
  // -2 because we left last token for the last assignment
  for(; result && (i <= tokens.length-2 ); i++){
    result[ tokens[i] ] = result[ tokens[i] ] || {};
    result = result[ tokens[i] ];
  }
  result[ tokens[i] ] = value;
  return obj;
}




/**
 * delete object property by point-divided selector like 'book.meta.author'
 * Also delete all parent properties if they are empty
 * @param {T} obj
 * @param {string|Array<string>} path
 * @param {boolean} [deleteEmpty:true]
 * @param {string} [separator:'.']
 *
 * @return {object} updated object
 */
export function xpath_delete(obj: any, path: (string|Array<string>), deleteEmpty: boolean = true, separator: string = '.'): any {
    let tokens = Array.isArray(path) ? path : path.split(separator);
    let result = obj;
    let deleteFrom = obj;
    let deleteFromIndex = 0;

    let i=0;
    // -2 because we left last token for deletion
    for(; i <= tokens.length-2; i++){
      result = result[ tokens[i] ];
      if(!result){
        // no element
        return obj;
      }

      //
      if (!deleteEmpty || Object.keys(result).length > 1){
        deleteFrom = result;
        deleteFromIndex = i;
      }
    }

    //
    if (!deleteEmpty || Object.keys(result).length > 1){
      deleteFrom = result;
      deleteFromIndex = i;
    }

    delete deleteFrom[ tokens[deleteFromIndex] ];
    return obj;
  }




/**
 *
 */
export class Reducer {

  /**
   * @param {string} key
   * @returns {(group, item: any) => any}
   */
  static groupByKey(key: string) {
    return function (group, item: any) {
      const kval = item[key];
      group[kval] = group[kval] || [];
      group[kval].push(item);
      return group;
    };
  };

}

