/**Log
 * Log function result
 */
export function Log(category?: string) {

  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {

    category = category || `${target.constructor.name}:${propertyKey}`;

    // logger function
    function log(data: any) {
      console.log(category, data);
    }


    const originalFunction = descriptor.value;
    descriptor.value = function (...args): Promise<any> {
      const caller = this;

      const resultProposal = originalFunction.apply(caller, args);

      if (resultProposal && typeof resultProposal.then === "function") {
        // result is a promise
        return resultProposal.then(any => {
          log(any);
          return any;
        });
      } else {
        log(resultProposal);
        return resultProposal;
      }


    }

  };

} // -
