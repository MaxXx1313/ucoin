


/**
 * @param {string} property
 * @returns {(target: any, propertyKey: string, descriptor: PropertyDescriptor) => void}
 * @constructor
 */
export function PromiseFlag(property: string) {

  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalFunction = descriptor.value;

    if (!target.hasOwnProperty(property)) {
      console.warn('PromiseFlag: property not visible: ', `${target.constructor.name}:${property}`);
    }

    descriptor.value = function(...args): Promise<any> {
      const caller = this;

      target[property] = true;
      return Promise.resolve( originalFunction.apply(caller, args) )
        .then(any => {
          target[property] = false;
          return any;
        }).catch(e => {
          target[property] = false;
          throw e
        });

    }

  };

} // -
