import { Observable } from "rxjs";



/**
 * Log function result
 */
export function LogObservable(category?: string) {

  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {

    category = category || `${target.constructor.name}:${propertyKey}`;

    // logger function
    function log(data: any) {
      console.log('%c%s', 'color: #bada55', category, data);
    }

    const originalFunction = descriptor.value;

    descriptor.value = function (...args): Observable<any> {

      return originalFunction.apply(this, args).map( data => {
        log(data);
        return data;
      });

    };

  };

} // -
