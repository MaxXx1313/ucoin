# The uCoin project

Unofficial application for uCoin.net - an International Catalog of World Coins. 



Android app is available **[here](https://tsfr.io/by.maxxx1313.ucoin)**



![screen-1](doc/screen-1.png) ![screen-1](doc/screen-3.png)



## Table of Contents

1. [developing](#developing) 
2. [testing](#testing) 

## Developing

```bash
# run once
npm install -g ionic
npm install
```

```bash
# console #1
ionic serve

# console #2
./proxy-serer
```

## Testing

Test stuff are based on [this](
https://github.com/ionic-team/ionic-unit-testing-example) article.


```bash
npm run test-ci
```




