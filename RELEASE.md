

Build release file
==================

Requirements
------

You need to have sign certificate.

### Create certificate 

```bash
keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000 -deststoretype pkcs12 -storepass "changeit"
```


Build android app
------


```bash
npm run release
```

See the result in `release` folder


See [official documenation](https://ionicframework.com/docs/v1/guide/publishing.html)
